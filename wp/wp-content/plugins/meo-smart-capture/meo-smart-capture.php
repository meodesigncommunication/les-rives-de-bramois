<?php
/*
Plugin Name: MEO Smart Capture
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited.
Proprietary and confidential

info@allmeo.com
*/
define('MSC_TEXT_DOMAIN', 'meo-smart-capture');
define('MSC_MIN_PHP_VERSION', '5.0');

$required_plugins = array('contact-form-7/wp-contact-form-7.php');

// Allowed MIME TYPE (PDF, XLS)
global $meoMIMEtypes;
$meoMIMEtypes = array("application/pdf", "application/vnd.ms-excel", "application/msexcel",
					"application/x-msexcel", "application/x-ms-excel", "application/x-excel", 
					"application/x-dos_ms_excel", "application/xls", "application/x-xls",
					"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

function meosc_showversionnotice() {
	echo '<div class="updated fade">' .
		__('Error: plugin "MEO Smart Capture" requires a newer version of PHP to be running.',  MSC_TEXT_DOMAIN).
		'<br/>' . __('Minimal version of PHP required: ', MSC_TEXT_DOMAIN) . '<strong>' . MSC_MIN_PHP_VERSION . '</strong>' .
		'<br/>' . __('Your server\'s PHP version: ', MSC_TEXT_DOMAIN) . '<strong>' . phpversion() . '</strong>' .
		'</div>';
}

function meosc_phpversioncheck() {
	if (version_compare(phpversion(), MSC_MIN_PHP_VERSION) < 0) {
		add_action('admin_notices', 'meosc_showversionnotice');
		return false;
	}
	return true;
}

function meosc_missingplugin() {
	global $required_plugins;
	echo '<div class="updated fade">' .
		__('Error: plugin "MEO Smart Capture" depends on the following" plugins.  ',  MSC_TEXT_DOMAIN).
		__('Please install and/or activate them', MSC_TEXT_DOMAIN) .
		'<br/>' . join('<br/>', $required_plugins) .
		'</div>';
}

function meosc_plugindependencycheck() {
	global $required_plugins;
	if (empty($required_plugins)) {
		return true;
	}
	if (!function_exists('is_plugin_active')) {
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	}
	foreach ($required_plugins as $required_plugin) {
		if (!is_plugin_active($required_plugin)) {
			add_action('admin_notices', 'meosc_missingplugin');
			return false;
		}
	}
	return true;
}

function msc_scripts_and_styles() {
	if ( !is_admin() ) {
		wp_enqueue_script('cookie', plugins_url('js/jquery.cookie.js', __FILE__), array('jquery'), '1.4.1', true);
		wp_enqueue_script('msc', plugins_url('js/meo-smart-capture.js', __FILE__), array('jquery', 'cookie'), filemtime(dirname(__FILE__) . '/js/meo-smart-capture.js'), true);
	}
}


function meosc_load_pluggables() {
	require_once('meosc-pluggable.php');
}

$dependencies_present = meosc_plugindependencycheck();
$dependencies_present = meosc_phpversioncheck() && $dependencies_present;

// If all the dependencies are there, we're good to go
if ($dependencies_present) {
	global $meosc;

	$plugin_dir = dirname(plugin_basename(__FILE__));
	load_plugin_textdomain(MSC_TEXT_DOMAIN, false, $plugin_dir . '/languages/');

	$plugin_root = plugin_dir_path( __FILE__ );

	require_once( $plugin_root . 'classes/class-meo-smart-capture.php');
	require_once( $plugin_root . 'classes/class-page-templater.php');

	// Installation and uninstallation hooks
	register_activation_hook(__FILE__, array('MeoSmartCapture', 'activate'));
	register_deactivation_hook(__FILE__, array('MeoSmartCapture', 'deactivate'));

	$meosc = new MeoSmartCapture();

	add_action( 'wp_enqueue_scripts', 'msc_scripts_and_styles' );
	add_action( 'plugins_loaded', array( 'PageTemplater', 'getInstance' ) ); // Allow wordpress to pull templates from this plugin
	add_action( 'wp', 'meosc_load_pluggables', 10);  // Ensure helper functions are defined
	add_action( 'after_setup_theme', array($meosc, 'createConfigPage') );
	add_action( 'wp_head', array($meosc, 'addPiwikCode') );
}
