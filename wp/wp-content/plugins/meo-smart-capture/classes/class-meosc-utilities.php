<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoScCf7Utilities {

	public function __construct() {
	}

	// Obfuscate attachment ID so knowledgeable WordPress
	// users can't download it directly knowing the ID
	public function encodeAttachmentId($attachment_id) {
		return $attachment_id * 17 + 3;
	}

	public function decodeAttachmentId($attachment_id) {
		return ( $attachment_id - 3 ) / 17;
	}

	public function mscCf7AccentedSort($string) {
		// Ugly, but windows won't sort â after a.
		// http://stackoverflow.com/questions/832709/natural-sorting-algorithm-in-php-with-support-for-unicode
		return preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|tilde|uml);~i', '$1' . chr(255) . '$2', htmlentities($string, ENT_QUOTES, 'UTF-8'));
	}

	public function getPostIdForTemplate($template_file) {
		global $wpdb;

		$sql = "select pm.post_id
		          from $wpdb->postmeta pm
		         where pm.meta_key = '_wp_page_template'
		           and pm.meta_value = %s
		         limit 1";

		$sql = $wpdb->prepare($sql, $template_file);

		return $wpdb->get_var($sql);
	}

	public function escapeHtml($in) {
		$translation_table = get_html_translation_table( HTML_ENTITIES, ENT_NOQUOTES );
		$translation_table[chr(38)] = '&';
		unset($translation_table['<']);
		unset($translation_table['>']);

		return strtr($in, $translation_table);
	}
}
