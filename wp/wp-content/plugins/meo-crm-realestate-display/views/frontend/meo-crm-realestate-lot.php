<?php
/* 
 * Template name: MEO CRM REALESTATE Lot
 */
global $wpdb;
$lot_id = $_GET['id'];
$upload_path = wp_upload_dir();
$detect = new Mobile_Detect();
$lot = RealestateModel::selectLotById($lot_id); // get lot datas

$data = array();
$data = Timber::get_context();
$data['posts'] = Timber::get_posts();
$data['page'] = 'Lot detail';
$data['plugin_path'] = plugins_url();
$data['lot'] = $lot;
$data['base_upload_url'] = $upload_path['baseurl'].'/';
$data['base_upload_dir'] = $upload_path['basedir'].'/';
$data['template_path'] = get_template_directory_uri();
$data['previous_page'] = site_url('list-lots');
$data['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));

Timber::render('twig/meo-crm-realestate-lot.html.twig', $data);