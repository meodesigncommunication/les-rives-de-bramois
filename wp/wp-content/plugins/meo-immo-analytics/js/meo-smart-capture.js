(function($){
	$(function() {
		var $forms = $('form.wpcf7-form');
		if ($forms.length === 0 || typeof $.cookie !== 'function') {
			return;
		}

		var previousvalues = $.cookie('smartcapture');
		if (!previousvalues) {
			return;
		}

		previousvalues = JSON.parse(previousvalues);
		if (previousvalues){
			for(var name in previousvalues){
				$forms.find('input[name='+name+'], select[name='+name+']').val(previousvalues[name]);
			}
		}
	});
})(jQuery);


smartcapturesubmit = function(eventname, data) {

	// Notify analytics that the form's been submitted successfully
	if (typeof track !== 'undefined' && eventname) {
		track(eventname, document.title);
	}
	else {
		console.log('File downloaded');
	}

	var post_download = jQuery('.post-download'),
	    form = jQuery(data.into);
	if (post_download.length) {
		var email_field = form.find('input[name="email"]'),
		    email = email_field.val();

		post_download.find('.sent-to').html(email);

		jQuery('.download-form').hide();
		post_download.show();
	}

	if (typeof jQuery.cookie === 'function') {
		var form_data = {};
		form.find('input[type=text], input[type=email], select').each(function(){
			form_data[jQuery(this).attr('name')] = jQuery(this).val();
		});

		jQuery.cookie('smartcapture', JSON.stringify(form_data), { expires: 7, path: '/' });
	}
};
