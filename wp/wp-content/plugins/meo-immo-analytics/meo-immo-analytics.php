<?php 

/*
Plugin Name: MEO CRM Analytics
Description: Plugin to visualize Google Analytics statistics. Requires MEO CRM Core plugin, Contact Form 7 and Contact Form 7 Db
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sarl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

$plugin_root = plugin_dir_path( __FILE__ );

# Defines / Constants
define('MEO_ANALYTICS_PLUGIN_ROOT', 	$plugin_root);
define('MEO_ANALYTICS_PLUGIN_SLUG', 	'meo-immo-analytics');
define('MEO_ANALYTICS_SLUG', 			'meo-immo-analytics');			// Analytics page slug (Frontend)
define('MEO_ANALYTICS_FILEDLDER_TABLE', 'meo_immo_file_downloader');	// Analytics db table
define('MEO_ANALYTICS_FILEDL_TABLE', 	'meo_immo_file_download');		// Analytics db table
define('MEO_ANALYTICS_SERVER', 			'piwik.meoanalytics.com');		// TODO: add dev server

define('MEO_IMMO_ANALYTICS_ATTREQ_FRONT_SLUG', 		'file-request');
define('MEO_IMMO_ANALYTICS_ATTREQ_TPL_FRONT_NAME', 	'msc-attachment-request.php');
define('MEO_IMMO_ANALYTICS_ATTREQ_TPL_FRONT', 		'../../'.MEO_ANALYTICS_PLUGIN_SLUG.'/views/frontend/'.MEO_IMMO_ANALYTICS_ATTREQ_TPL_FRONT_NAME);
define('MEO_IMMO_ANALYTICS_ATTDLD_FRONT_SLUG', 		'file-download');
define('MEO_IMMO_ANALYTICS_ATTDLD_TPL_FRONT_NAME', 	'msc-attachment-download-handler.php');
define('MEO_IMMO_ANALYTICS_ATTDLD_TPL_FRONT', 		'../../'.MEO_ANALYTICS_PLUGIN_SLUG.'/views/frontend/'.MEO_IMMO_ANALYTICS_ATTDLD_TPL_FRONT_NAME);

// define('MEO_IMMO_ANALYTICS_PIWIK_URL', 'http://piwik.meoanalytics.com/piwik.php');


# Required Files
require_once( $plugin_root . 'views/backend/meo-immo-analytics-view.php');
require_once( $plugin_root . 'classes/class-meo-smart-capture.php');
require_once( $plugin_root . 'classes/class-meosc-utilities.php');
require_once( $plugin_root . 'classes/class-meosc-cf7-integration.php');

# Globals
global $meo_immo_dlder_db_version, $meo_immo_dl_db_version, $meoMIMEtypes, $immoOptions;

$meo_immo_dlder_db_version = '1.0';
$meo_immo_dl_db_version = '1.0';

// Allowed MIME TYPES
$meoMIMEtypes = array(
						"application/pdf", "application/vnd.ms-excel", "application/msexcel",
						"application/x-msexcel", "application/x-ms-excel", "application/x-excel",
						"application/x-dos_ms_excel", "application/xls", "application/x-xls",
						"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

$immoOptions = array(
		'gtm_id' 		    => array( "type" => "text",  	"label" => "Google GTM ID", 			    "notes" => "" ),
		'google_id' 		=> array( "type" => "text",  	"label" => "Google Analytics ID", 			"notes" => "" ),
		'piwik_site_id' 	=> array( "type" => "text",  	"label" => "Piwik Site Id", 				"notes" => "" ),
		'api_key' 			=> array( "type" => "text",  	"label" => "Smart Capture API Key", 		"notes" => "Generate a random API key, eg<br/>Go to <a href='http://www.md5.cz/' target='_blank'>md5.cz (new tab will open)</a>, type a random string, take the first 16 characters of the result.<br/>Must be the same as set on the website written below" ),
		'smart_capture_url' => array( "type" => "text",  	"label" => "Smart Capture URL", 			"notes" => "Set to corresponding MEO Realestate site" ),
		'email_sender' 		=> array( "type" => "email", 	"label" => "Email Sender", 					"notes" => "" ),
		'email_sender_name' => array( "type" => "text",  	"label" => "Email Sender Name", 			"notes" => "" ),
		'email_bcc'			=> array( "type" => "email", 	"label" => "Email Bcc (comma separated)", 	"notes" => "" ),
		'email_subject'		=> array( "type" => "text", 	"label" => "Email Subject", 				"notes" => "" ),
		'email_content'		=> array( "type" => "textarea", "label" => "Email Content", 				"notes" => " (Instructions: Replaceable fields: [download_url] [lot_code] [logo] [sender_email])" ),
		'logo'				=> array( "type" => "text",  	"label" => "Logo URL",						"notes" => "" ),
);

/*
 * Check for Dependencies :: MEO IMMO Analytics
 */
function meo_immo_analytics_activate() {
	
	global $immoOptions;

	$installed_dependencies = false;
	if ( is_plugin_active( 'meo-crm-core/meo-crm-core.php' ) && is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) 
			&& is_plugin_active( 'contact-form-7-to-database-extension/contact-form-7-db.php' ) ) {
		$installed_dependencies = true;
	}

	if(!$installed_dependencies) {

		// WordPress check for fatal error while activating plugin, so simplest solution will be trigger a fatal error
		// and this will prevent WordPress to activate the plugin.
		echo '<div class="notice notice-error"><h3>'.__('Please install and activate the MEO CRM Core, Contact Form 7 and Contact Form 7 Db plugins before', 'meo-realestate').'</h3></div>';

		//Adding @ before will prevent XDebug output
		@trigger_error(__('Please install and activate the MEO CRM Core, Contact Form 7 and Contact Form 7 Db plugins before.', 'meo-realestate'), E_USER_ERROR);
		exit;


	}
	else {

		// Everything is fine

		global $wpdb, $meo_immo_dlder_db_version, $meo_immo_dl_db_version;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		$table_name_dlder = $wpdb->prefix . MEO_ANALYTICS_FILEDLDER_TABLE;
		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE " . $table_name_dlder . " (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					last_name varchar(100) NOT NULL,
					first_name varchar(100) NOT NULL,
					email varchar(100) NOT NULL,
					phone varchar(100) NOT NULL,
					address varchar(100) NOT NULL,
					postcode varchar(100) NOT NULL,
					city varchar(100) NOT NULL,
					country varchar(100) NOT NULL,
					language varchar(100) NOT NULL,
					creation_date datetime DEFAULT NULL,
					PRIMARY KEY (id),
					UNIQUE KEY email (email)
					) $charset_collate;";
		
		dbDelta( $sql );
		
		add_option( 'meo_immo_dlder_db_version', $meo_immo_dlder_db_version );

	
		
		$table_name_dl = $wpdb->prefix . MEO_ANALYTICS_FILEDL_TABLE;
		
		$sql = "CREATE TABLE " . $table_name_dl . " (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					file_downloader_id bigint(20) unsigned NOT NULL,
					attachment_id bigint(20) unsigned DEFAULT NULL,
					download_count int(11) NOT NULL DEFAULT '0',
					download_code varchar(20) NOT NULL,
					PRIMARY KEY (id),
					KEY file_download_file_downloader_fk (file_downloader_id),
					CONSTRAINT file_download_file_downloader_fk FOREIGN KEY (file_downloader_id) REFERENCES " . $table_name_dlder . " (id) ON UPDATE CASCADE
							) $charset_collate;";
		
		dbDelta( $sql );

		// Update db with the version info
		add_option( 'meo_immo_dl_db_version', $meo_immo_dl_db_version );
		
		// Prepare options
		foreach ($immoOptions as $immoOption => $optionDetail){
			add_option($immoOption, '');
		}
		
	}

}
register_activation_hook( __FILE__, 'meo_immo_analytics_activate' );


// Working with classes
$meosc = new MeoSmartCapture();
//$meosc->utilities = new MeoScCf7Utilities();
//$meosc->cf7integration = new MeoScCf7Integration($meosc, $meosc->utilities);

# Add Scripts and Styles

add_action( 'wp_enqueue_scripts', 'meo_immo_analytics_scripts_styles' );

function meo_immo_analytics_scripts_styles() {

	# JS (For convinience, there is a .js.php file available, to build dynamically the charts JS code)
	wp_register_script( 'meo_immo_analytics_cookie_js',  plugins_url('js/jquery.cookie.js', __FILE__), false, '1.4.1' );
	wp_enqueue_script( 'meo_immo_analytics_cookie_js' );
	
	wp_register_script( 'meo_immo_analytics_sc_js',  plugins_url('js/meo-smart-capture.js', __FILE__), false, '1.0.0' );
	wp_enqueue_script( 'meo_immo_analytics_sc_js' );

}

#########
# ADMIN #
#########


# Admin Menu
add_action( 'admin_menu', 'meo_immo_analytics_admin_menu' );

function meo_immo_analytics_admin_menu() {
	add_menu_page( 'MEO Immo Analytics',
			'Analytics',
			'manage_options',
			MEO_ANALYTICS_PLUGIN_SLUG,
			'meo_immo_analytics_admin_page',
			'dashicons-chart-line',
			7  );
}
	
	# Add plugin submenu in admin navigation
	
	add_action( 'admin_menu', 'meo_immo_analytics_admin_submenu', 100 );
	
	function meo_immo_analytics_admin_submenu() {
		add_submenu_page(MEO_ANALYTICS_PLUGIN_SLUG,
				'Piwik',
				'Piwik',
				'manage_options',
				'analytics_piwik',
				'add_page_analytics_piwik');
	}
	
	# Admin page to visualize users availability
	
	function add_page_analytics_admin() {
		include_once 'views/backend/meo-immo-analytics-view.php';
		meo_immo_analytics_admin_page();
	}
	

# Admin Page

	# Fire our meta box setup function on the post editor screen
	add_action( 'load-post.php', 		'meo_immo_analytics_post_meta_boxes_setup' );
	add_action( 'load-post-new.php', 	'meo_immo_analytics_post_meta_boxes_setup' );
	
	function meo_immo_analytics_post_meta_boxes_setup() {
	
		# Add Meta Box to add Price List Container boolean meta
		/* This meta will set the $_SESSION["pricesEnabled"] var, if the user download something related to the price list */
		add_action( 'add_meta_boxes', 'meo_immo_analytics_meta_box_add' );
		
		# Save post meta
		add_filter('attachment_fields_to_save', 'meo_immo_analytics_save_post_meta');
	
	}
	
	# Adding meta box
	function meo_immo_analytics_meta_box_add() {
	
		add_meta_box( 'meo_immo_analytics_price_enabler_id', 'Price Enabler', 'meo_immo_analytics_price_enabler', 'attachment', 'normal', 'high' );
		
	}
		// Function callback
		function meo_immo_analytics_price_enabler() {
			
			global $post;
			
			if(get_post_meta($post->ID, "_price_enabler", true)) { $checked = 'checked="checked"'; }
			else $checked = '';
			
			wp_nonce_field( basename( __FILE__ ), 'meo_immo_analytics_price_enabler_nonce' ); ?>
		    <label for="meo_immo_analytics_price_enabler_meta_box_text">Price Enabler</label>
		    <input type="checkbox" name="meo_immo_analytics_price_enabler_meta" id="meo_immo_analytics_price_enabler_meta" <?php echo $checked; ?> />
		    <?php    
		}
		
	# Saving the meta data
	function meo_immo_analytics_save_post_meta( $return ) {
		
		global $post;
		
		$post_id = $post->ID;
		
		/* Verify the nonce before proceeding. */
		if ( !isset( $_POST['meo_immo_analytics_price_enabler_nonce'] ) || !wp_verify_nonce( $_POST['meo_immo_analytics_price_enabler_nonce'], basename( __FILE__ ) ) )
			return $post_id;
		
		/* Get the post type object. */
		$post_type = get_post_type_object( $post->post_type );
	
		/* Check if the current user has permission to edit the post. */
		if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
			return $post_id;
		
		/* Get the posted data and sanitize it for use as an HTML class. */
		$new_meta_value = ( isset( $_POST['meo_immo_analytics_price_enabler_meta'] ) ? 1 : 0 );
	
		/* Get the meta key. */
		$meta_key = '_price_enabler';
	
		/* Get the meta value of the custom field key. */
		$meta_value = get_post_meta( $post_id, $meta_key, true );
	
		/* If a new meta value was added and there was no previous value, add it. */
		if ( $new_meta_value && '' == $meta_value )
			add_post_meta( $post_id, $meta_key, $new_meta_value, true );
	
		/* If the new meta value does not match the old value, update it. */
		elseif ( $new_meta_value && $new_meta_value != $meta_value )
		update_post_meta( $post_id, $meta_key, $new_meta_value );
	
		/* If there is no new meta value but an old value exists, delete it. */
		elseif ( '' == $new_meta_value && $meta_value )
		delete_post_meta( $post_id, $meta_key, $meta_value );
		
		return $return;
	}
	
	# Condition to avoid to print twice the field on the media edition page
	if($_SERVER['REDIRECT_SCRIPT_URL'] != '/wp/wp-admin/post.php'){
		/* For adding custom field to gallery popup */
		function add_image_attachment_fields_to_edit($form_fields, $post) {
			// $form_fields is a an array of fields to include in the attachment form
			// $post is nothing but attachment record in the database
			//     $post->post_type == 'attachment'
			// attachments are considered as posts in WordPress. So value of post_type in wp_posts table will be attachment
			// now add our custom field to the $form_fields array
			// input type="text" name/id="attachments[$attachment->ID][custom1]"
			if(get_post_meta($post->ID, "_price_enabler", true)) { $checked = 'checked="checked"'; }
			else $checked = '';
			
			$html = '<input type="checkbox" name="price_enabler" id="price_enabler" '.$checked.' /><span class="meo-immo-analytics-message"></span>
			

			<script type="text/javascript">
			jQuery(document).ready(function(){
			
				jQuery("#price_enabler").click(function() {
					var $this = jQuery(this);
					// $this will contain a reference to the checkbox
					var price_enabler_check = 0;
					if ($this.is(":checked")) {
						price_enabler_check = 1;
					}
								
					// Ajax code
					var data = {
						"action": "meo_immo_analytics_price_enabler_ajax",
						"postID": '.$post->ID.',
						"price_enabler" : price_enabler_check
					};
					jQuery.post("'.admin_url( 'admin-ajax.php' ).'", data, function(response) {
						jQuery(".meo-immo-analytics-message").html(" ");
						jQuery(".meo-immo-analytics-message").fadeIn(1000);
						jQuery(".meo-immo-analytics-message").html(response);
						jQuery(".meo-immo-analytics-message").fadeOut(4000);
					});
				
				});
			});
			</script>';
		
			$form_fields["price_enabler"] = array(
					"label" => __("Price Enabler"),
					"input" => "html", // this is default if "input" is omitted
					"html" => $html,
					// "value" => get_post_meta($post->ID, "_price_enabler", true),
					"helps" => __("Check this box to reveal prices over the site, when this media is downloaded."),
			);
			return $form_fields;
		}
		// now attach our function to the hook
		add_filter("attachment_fields_to_edit", "add_image_attachment_fields_to_edit", null, 2);
		
		function add_image_attachment_fields_to_save($post, $attachment) {
			// $attachment part of the form $_POST ($_POST[attachments][postID])
			// $post['post_type'] == 'attachment'
			if( isset($attachment['price_enabler']) ){
				// update_post_meta(postID, meta_key, meta_value);
				update_post_meta($post['ID'], '_price_enabler', $attachment['price_enabler']);
			}
			return $post;
		}
		// now attach our function to the hook.
		add_filter("attachment_fields_to_save", "add_image_attachment_fields_to_save", null , 2);
	}
	
	# Ajax meta box price enabler
	add_action( 'wp_ajax_meo_immo_analytics_price_enabler_ajax', 'meo_immo_analytics_price_enabler_ajax_callback' );
	add_action( 'wp_ajax_nopriv_meo_immo_analytics_price_enabler_ajax', 'meo_immo_analytics_price_enabler_ajax_callback' );
	
	function meo_immo_analytics_price_enabler_ajax_callback() {
		
		update_post_meta( $_POST['postID'], '_price_enabler', $_POST['price_enabler'] );
		
		echo 'Saved';
		exit();
	}
	

#########
# FRONT #
#########


# PAGE CREATION

	# Register Plugin templates
	
	add_filter('meo_crm_core_templates_collector', 'meo_immo_analytics_templates_register', 1, 1);
	
	function meo_immo_analytics_templates_register($pluginsTemplates){

		$pluginsTemplates[MEO_IMMO_ANALYTICS_ATTREQ_TPL_FRONT] = 'MEO Attachment Request';
		$pluginsTemplates[MEO_IMMO_ANALYTICS_ATTDLD_TPL_FRONT] = 'MEO Attachment Download Handler';
	
		return $pluginsTemplates;
	}
	
	# Register Plugin Pages
	
	add_filter( 'meo_crm_core_front_pages_collector', 'meo_immo_analytics_pages_register' );
	
	function meo_immo_analytics_pages_register($pluginsPages) {
	
		# Create Page for Analytics
		$pluginsPages[MEO_IMMO_ANALYTICS_ATTREQ_FRONT_SLUG] = array(	
						'post_title' 		=> 'MEO IMMO Analytics - File Request',
						'post_content' 		=> 'Replace this content by Contact Form 7 Shortcode.',
						'_wp_page_template' => MEO_IMMO_ANALYTICS_ATTREQ_TPL_FRONT);
		
		# Create Page for Analytics
		$pluginsPages[MEO_IMMO_ANALYTICS_ATTDLD_FRONT_SLUG] = array(	
						'post_title' 		=> 'MEO IMMO Analytics - File Download',
						'post_content' 		=> 'No content.',
						'_wp_page_template' => MEO_IMMO_ANALYTICS_ATTDLD_TPL_FRONT);
		
		return $pluginsPages;
	}
	
	# Tracking codes
	
	if(defined('IS_PRODUCTION') && IS_PRODUCTION) {
		add_action( 'wp_head', 'meo_immo_analytics_piwik_tracking_code' );
		add_action('wp_head', 'meo_immo_analytics_google_tracking_code');
	}

	# Piwik Tracking Code
	
	function meo_immo_analytics_piwik_tracking_code() {
		
		// Code was originally in meo-realestate-development-plugin
		
		global $wp_query;
		
		$piwik_site_id = get_option('piwik_site_id');
		
		if (empty($piwik_site_id)) {
			return;
		}
		
		// Check if pricelist has been downloaded
		$prices_enabled = MeoScCf7Integration::isPriceEnabled();
		// Get page slug from postId
		$template = get_page_template_slug( $wp_query->post->ID );
		
		$append_price_to_title = $prices_enabled; //  && ($template == "plan-list.php" || $wp_query->post->post_type == DevelopmentDaoP2P::CPT_LOT);
		?>
				<!-- Piwik -->
				<script type="text/javascript">
					var analytics_post_id = <?php echo $wp_query->post->ID; ?>,
					    analytics_post_type = '<?php echo $wp_query->post->post_type; ?>',
					    analytics_prices = '<?php echo $prices_enabled ? "true" : "false"; ?>';
			
					var triesCounter = 0;
					piwikload = function() {
						if (typeof jQuery === "undefined") {
							triesCounter++;
							if (triesCounter < 20) {
								setTimeout(piwikload, 50);
							}
						}
						else {
							var $forms = jQuery('form.wpcf7-form');
							if ($forms.length !== 0) {
								$forms.each(function () {
									jQuery('<input>').attr({
										type: 'hidden',
										name: 'analytics_id',
										value: "" + piwik_user_id
									}).appendTo(jQuery(this));
								});
							}
						}
					};
					var _paq = _paq || [];
					_paq.push(['setCustomVariable', '1', 'post_id',        analytics_post_id,   "page"]);
					_paq.push(['setCustomVariable', '2', 'post_type',      analytics_post_type, "page"]);
					_paq.push(['setCustomVariable', '3', 'prices_enabled', analytics_prices,    "page"]);
					_paq.push(['trackPageView'<?php echo $append_price_to_title ? ', document.title + " ($)"' : ''; ?>]);
					_paq.push(['enableLinkTracking']);
					_paq.push([ function() {
						piwik_user_id = this.getVisitorId();
						piwikload();
					}]);
					(function() {
						var u=(("https:" == document.location.protocol) ? "https" : "http") + "://<?php echo MEO_ANALYTICS_SERVER; ?>/";
						_paq.push(['setTrackerUrl', u+'piwik.php']);
						_paq.push(['setSiteId', <?php echo $piwik_site_id; ?>]);
						var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
						g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
					})();
				</script>
				<noscript><p><img src="http://<?php echo MEO_ANALYTICS_SERVER; ?>/piwik.php?idsite=<?php echo $piwik_site_id; ?>" style="border:0;" alt="" /></p></noscript>
				<!-- End Piwik Code --><?php
	}
	
	# Google Analytics Tracking Code
	
	function meo_immo_analytics_google_tracking_code() {
		
		$googleID = get_option('google_id');
		
		if($googleID != '' && !is_preview()){
			echo "<script>
				  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
				
				  ga('create', '".$googleID."', 'auto');
				  ga('require', 'GTM-TL5HR6G');
				  ga('send', 'pageview');
				
				</script>";
		}else{
			echo "<script>
				  alert('erreur');
				</script>";
		}
		
	}