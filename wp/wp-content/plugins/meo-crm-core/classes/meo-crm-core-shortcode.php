<?php

/*
 * 
 */
class MeoCrmCoreShortcode
{
    
    public function __construct(){ 
        add_action( 'wpcf7_init', array( $this, 'setCF7PreviousPageShortcode' ) );
    }
    
    # Set CF7 country shortcode to implement into form
    public function setCF7PreviousPageShortcode() {
        wpcf7_add_shortcode( 'meo_crm_core_previous_page_cf7', array( $this, 'setCF7PreviousPageShortcodeHandler') );
    }

    # Set Action when interpreting the defined shortcode
    public function setCF7PreviousPageShortcodeHandler() {
        $detect = new Mobile_Detect();
        if($detect->isMobile() && !$detect->isTablet())
        {
            $html = '';
            $url = $_SERVER['HTTP_REFERER'];

            if((isset($_POST['previous_url']) && !empty($_POST['previous_url'])) && !empty($url))
            {
                $html .= '<input type="hidden" name="previous_url" id="previous_url" value="'.$_POST['previous_url'].'" />
                          <a id="previous-page-button" href="'.$_POST['previous_url'].'" title="back to previous page">Fermer</a>';
            }else{
                $html .= '<input type="hidden" name="previous_url" id="previous_url" value="'.$url.'" />
                          <a id="previous-page-button" href="'.$url.'" title="back to previous page">Fermer</a>';
            }
            return $html;
        }else{
            return '';
        }
    }
    
}

