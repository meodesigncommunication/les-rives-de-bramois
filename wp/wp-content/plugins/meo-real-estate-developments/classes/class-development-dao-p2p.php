<?php
require_once(dirname(__FILE__) . '/../interfaces/interface-development-dao.php');

class DevelopmentDaoP2P implements DevelopmentDao {

	// Custom post types
	const CPT_DEVELOPMENT = 'development';
	const CPT_SECTOR      = 'sector';
	const CPT_BUILDING    = 'building';
	const CPT_FLOOR       = 'floor';
	const CPT_PLAN        = 'plan';
	const CPT_LOT         = 'lot';
	const CPT_ENTRY       = 'entry';

	// Custom taxonomies
	const TAX_LOT_TYPES   = 'lot_type';

	// Relationships
	const REL_SECTOR_DEV      = 'sectors_to_developments';
	const REL_BUILDING_SECTOR = 'buildings_to_sectors';
	const REL_FLOOR_BUILDING  = 'floors_to_buildings';
	const REL_LOT_FLOOR       = 'lots_to_floors';
	const REL_LOT_PLAN        = 'lots_to_plans';
	const REL_ENTRY_BUILDING  = 'entries_to_buildings';
	const REL_LOT_ENTRY       = 'lots_to_entries';

	private $developments = array();
	private $sectors = array();
	private $buildings = array();
	private $floors = array();
	private $lots = array();
	private $plans = array();
	private $entries = array();
	private $lot_types = array();

	private $qtranslate_active;
	private $acf_qtranslate_active;

	public function __construct() {
		if (!function_exists('is_plugin_active')) {
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}
		$this->qtranslate_active = ( is_plugin_active('qtranslate/qtranslate.php') || is_plugin_active('mqtranslate/mqtranslate.php') );
		$this->acf_qtranslate_active = is_plugin_active('acf-qtranslate/acf-qtranslate.php');

		add_action('init', array(&$this, 'createCustomPostTypes'), 5);
		add_action('p2p_init', array(&$this, 'createRelationships'));
	}

	public function reset(){
		global $post;

		$this->developments = array();
		$this->sectors = array();
		$this->buildings = array();
		$this->floors = array();
		$this->lots = array();
		$this->plans = array();
		$this->entries = array();
		$this->lot_types = array();

		return true;
	}

	public function getDevelopments($ids = array()) {
		if (!empty($this->developments) && empty($ids)) {
			return $this->developments;
		}

		// If we've already loaded all values, use the cache to return what we can
		$checked_ids = $this->sanitiseIds($ids);
		$result = $this->getCachedValues($this->developments, $checked_ids);
		if ($result != null) {
			return $result;
		}

		$result = $this->getTypeDetails(
			self::CPT_DEVELOPMENT,
			$checked_ids,
			$this->getDevelopmentFieldDefinitions(),
			array()
		);

		if (empty($ids)) {
			$this->developments = $result;
		}

		return $result;
	}

	public function getSectors($ids = array()) {
		if (!empty($this->sectors) && empty($ids)) {
			return $this->sectors;
		}

		// If we've already loaded all values, use the cache to return what we can
		$checked_ids = $this->sanitiseIds($ids);
		$result = $this->getCachedValues($this->sectors, $checked_ids);
		if ($result != null) {
			return $result;
		}

		$result = $this->getTypeDetails(
			self::CPT_SECTOR,
			$checked_ids,
			$this->getSectorFieldDefinitions(),
			array(
				'development' => self::REL_SECTOR_DEV
			)
		);

		if (empty($ids)) {
			$this->sectors = $result;
		}

		return $result;
	}

	public function getBuildings($ids = array()) {
		if (!empty($this->buildings) && empty($ids)) {
			return $this->buildings;
		}

		// If we've already loaded all values, use the cache to return what we can
		$checked_ids = $this->sanitiseIds($ids);
		$result = $this->getCachedValues($this->buildings, $checked_ids);
		if ($result != null) {
			return $result;
		}

		$result = $this->getTypeDetails(
			self::CPT_BUILDING,
			$checked_ids,
			$this->getBuildingFieldDefinitions(),
			array(
				'sector' => self::REL_BUILDING_SECTOR
			)
		);

		foreach ($result as $index => $building) {
			$lot_types = array();
			$lot_type_objects = get_the_terms($row->id, self::TAX_LOT_TYPES);
			if (!empty($lot_type_objects)) {
				foreach($lot_type_objects as $lot_type_object) {
					$lot_types[$lot_type_object->term_id] = array(
						'id'   => $lot_type_object->term_id,
						'name' => $lot_type_object->name,
						'slug' => $lot_type_object->slug
					);
				}
			}

			$result[$index]['lot_types'] = $lot_types;

		}

		if (empty($ids)) {
			$this->buildings = $result;
		}

		return $result;
	}

	public function getFloors($ids = array()) {
		if (!empty($this->floors) && empty($ids)) {
			return $this->floors;
		}

		// If we've already loaded all values, use the cache to return what we can
		$checked_ids = $this->sanitiseIds($ids);
		$result = $this->getCachedValues($this->floors, $checked_ids);
		if ($result != null) {
			return $result;
		}

		$result = $this->getTypeDetails(
			self::CPT_FLOOR,
			$checked_ids,
			$this->getFloorFieldDefinitions(),
			array(
				'building' => self::REL_FLOOR_BUILDING
			),
			'order'
		);

		if (empty($ids)) {
			$this->floors = $result;
		}

		return $result;
	}


	public function getLots($ids = array()) {
		global $wpdb;

		if (!empty($this->lots) && empty($ids)) {
			return $this->lots;
		}

		// If we've already loaded all values, use the cache to return what we can
		$checked_ids = $this->sanitiseIds($ids);
		$result = $this->getCachedValues($this->lots, $checked_ids);
		if ($result != null) {
			return $result;
		}

		$result = $this->getTypeDetails(
			self::CPT_LOT,
			$checked_ids,
			$this->getLotFieldDefinitions(),
			array(
				'plan' => self::REL_LOT_PLAN,
				'entry' => self::REL_LOT_ENTRY
			)
		);

		if (empty($result)) {
			return $result;
		}

		// REL_LOT_FLOOR is many - many, so it needs to be processed separately
		$sql = "select p.id as lot_id,
			           l2f.p2p_to as floor_id,
			           ent.meta_value as is_entry,
			           svg.meta_value as svg_id
			      from {$wpdb->posts} p
			           left join {$wpdb->prefix}p2p l2f on p.id = l2f.p2p_from and l2f.p2p_type = '" . self::REL_LOT_FLOOR . "'
			           left join {$wpdb->prefix}p2pmeta ent on l2f.p2p_id = ent.p2p_id and ent.meta_key = 'entry'
			           left join {$wpdb->prefix}p2pmeta svg on l2f.p2p_id = svg.p2p_id and svg.meta_key = 'svg_id'
			           left join {$wpdb->prefix}p2pmeta ord on l2f.p2p_id = ord.p2p_id and ord.meta_key = '_order_from'
			     where p.post_type = '" . self::CPT_LOT . "'
			       and p.post_status = 'publish'
			  order by p.id, ord.meta_value";

		$rows = $wpdb->get_results($sql);

		$lots_to_floors = array();
		foreach ($rows as $row) {
			if (!array_key_exists($row->lot_id, $lots_to_floors)) {
				$lots_to_floors[$row->lot_id] = array();
				$lots_to_floors[$row->lot_id]['multifloor'] = false;
				$lots_to_floors[$row->lot_id]['floor_id'] = $row->floor_id; // The floor the entry is on.  The lot's "main" floor
				$lots_to_floors[$row->lot_id]['all_floors'] = array();
				$floor_order = 1;
			}
			else {
				$lots_to_floors[$row->lot_id]['multifloor'] = true;
				$floor_order++;
			}

			if ($row->is_entry) {
				$lots_to_floors[$row->lot_id]['floor_id'] = $row->floor_id;
			}

			$lots_to_floors[$row->lot_id]['all_floors'][$row->floor_id] = array(
				'entry'  => $row->is_entry,
				'svg_id' => $row->svg_id,
				'order'  => $floor_order
			);
		}

		foreach ($result as $index => $lot) {
			$floor_details_exist = array_key_exists($index, $lots_to_floors);
			$result[$index]['multifloor'] = $floor_details_exist ? $lots_to_floors[$index]['multifloor'] : false;
			$result[$index]['floor_id']   = $floor_details_exist ? $lots_to_floors[$index]['floor_id']   : -1;
			$result[$index]['all_floors'] = $floor_details_exist ? $lots_to_floors[$index]['all_floors'] : array();

			// Lot types
			$lot_type = array();
			$lot_types = get_the_terms($lot->id, self::TAX_LOT_TYPES);
			if (!empty($lot_types)) {
				$first_lot_type = reset($lot_types); // Assume there's only one

				if (!empty($first_lot_type)) {
					$lot_type['id'] = $first_lot_type->term_id;
					$lot_type['name'] = $first_lot_type->name;
					$lot_type['slug'] = $first_lot_type->slug;
				}
			}
			$result[$index]['type'] = $lot_type;
		}

		if (empty($ids)) {
			$this->lots = $result;
		}

		return $result;
	}

	public function getPlans($ids = array()) {
		if (!empty($this->plans) && empty($ids)) {
			return $this->plans;
		}

		// If we've already loaded all values, use the cache to return what we can
		$checked_ids = $this->sanitiseIds($ids);
		$result = $this->getCachedValues($this->plans, $checked_ids);
		if ($result != null) {
			return $result;
		}

		$result = $this->getTypeDetails(
			self::CPT_PLAN,
			$checked_ids,
			$this->getPlanFieldDefinitions(),
			array()
		);

		if (empty($ids)) {
			$this->plans = $result;
		}

		return $result;
	}

	public function getEntries($ids = array()) {
		if (!empty($this->entries) && empty($ids)) {
			return $this->entries;
		}

		// If we've already loaded all values, use the cache to return what we can
		$checked_ids = $this->sanitiseIds($ids);
		$result = $this->getCachedValues($this->entries, $checked_ids);
		if ($result != null) {
			return $result;
		}

		$result = $this->getTypeDetails(
			self::CPT_ENTRY,
			$checked_ids,
			$this->getEntryFieldDefinitions(),
			array(
				'building' => self::REL_ENTRY_BUILDING
			)
		);

		if (empty($ids)) {
			$this->entries = $result;
		}

		return $result;
	}

	public function getLotTypes($exclusions = array(), $ids = array()) {
		if (!empty($this->lot_types)) {
			return $this->lot_types;
		}

		$raw_lot_types =  get_terms( self::TAX_LOT_TYPES, array(
			'hide_empty' => 0
		));

		// Could just return objects, but I'd rather be consistent
		// with other elements - so array
		$this->lot_types = array();
		foreach ($raw_lot_types as $raw_lot_type) {
			$lot_type = array(
				'id'   => $raw_lot_type->term_id,
				'name' => $raw_lot_type->name,
				'slug' => $raw_lot_type->slug
			);

			$this->lot_types[$raw_lot_type->term_id] = $lot_type;
		}

		return $this->lot_types;
	}


	/* ---------------------------------------------------------------------------------------------
	 * Setup functions
	 * --------------------------------------------------------------------------------------------- */
	public function createCustomPostTypes() {
		$this->createDevelopmentCustomPostType();
		$this->createSectorCustomPostType();
		$this->createBuildingCustomPostType();
		$this->createFloorCustomPostType();
		$this->createPlanCustomPostType();
		$this->createLotCustomPostType();
		$this->createEntryCustomPostType();

		$this->createLotTypeTaxonomy();
	}

	private function getMultilingualFieldType($type) {
		$equivalents = array(
			'text'     => 'qtranslate_text',
			'textarea' => 'qtranslate_textarea',
			'wysiwyg'  => 'qtranslate_wysiwyg',
			'image'    => 'qtranslate_image',
			'file'     => 'qtranslate_file',
		);
		if (!$this->acf_qtranslate_active or !isset($equivalents[$type])) {
			return $type;
		}
		return $equivalents[$type];
	}

	private function createDevelopmentCustomPostType() {
		register_post_type(self::CPT_DEVELOPMENT, array(
			'labels' => array(
				'name' => __('Developments', MRED_TEXT_DOMAIN),
				'singular_name' => __('Development', MRED_TEXT_DOMAIN),
				'add_new_item' => __('Add a development', MRED_TEXT_DOMAIN),
				'edit_item' => __('Edit development', MRED_TEXT_DOMAIN),
				'new_item' => __('Add a development', MRED_TEXT_DOMAIN),
				'search_items' => __('Find a development', MRED_TEXT_DOMAIN),
				'not_found' => __('No development found', MRED_TEXT_DOMAIN),
				'not_found_in_trash' => __('No development found in the trash', MRED_TEXT_DOMAIN),
			),
			'public' => true,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => array('title', 'editor')
		));

		if(function_exists("register_field_group"))
		{
			register_field_group(array (
				'id' => 'acf_development-details',
				'title' => 'Development details',
				'fields' => $this->getDevelopmentFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::CPT_DEVELOPMENT,
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}
	}

	public function getDevelopmentFieldDefinitions() {
		return array (
					array (
						'key' => 'field_537ccb680800d',
						'label' => 'Code',
						'name' => 'code',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_53fb19043142b',
						'label' => 'plan',
						'name' => 'plan',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_5418ab8a2c2cc',
						'label' => 'Transparent PNG of plan size',
						'name' => 'plan_hover_transparent',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
				);
	}

	private function createSectorCustomPostType() {
		$supports = array('title');

		// qTranslate won't fire up for other fields unless the editor is present
		if ($this->qtranslate_active) {
			$supports[] = 'editor';
		}

		register_post_type(self::CPT_SECTOR, array(
			'labels' => array(
				'name' => __('Sectors', MRED_TEXT_DOMAIN),
				'singular_name' => __('Sector', MRED_TEXT_DOMAIN),
				'add_new_item' => __('Add an sector', MRED_TEXT_DOMAIN),
				'edit_item' => __('Edit sector', MRED_TEXT_DOMAIN),
				'new_item' => __('Add an sector', MRED_TEXT_DOMAIN),
				'search_items' => __('Find an sector', MRED_TEXT_DOMAIN),
				'not_found' => __('No sector found', MRED_TEXT_DOMAIN),
				'not_found_in_trash' => __('No sector found in the trash', MRED_TEXT_DOMAIN),
			),
			'public' => false,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => $supports
		));

		if(function_exists("register_field_group"))
		{
			register_field_group(array (
				'id' => 'acf_sector-details',
				'title' => 'Sector details',
				'fields' => $this->getSectorFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::CPT_SECTOR,
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}
	}

	public function getSectorFieldDefinitions() {
		return  array (
			array (
				'key' => 'field_54dc9adc38ea6',
				'label' => 'Code',
				'name' => 'code',
				'prefix' => '',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			)
		);
	}

	private function createBuildingCustomPostType() {
		register_post_type(self::CPT_BUILDING, array(
			'labels' => array(
				'name' => __('Buildings', MRED_TEXT_DOMAIN),
				'singular_name' => __('Building', MRED_TEXT_DOMAIN),
				'add_new_item' => __('Add a building', MRED_TEXT_DOMAIN),
				'edit_item' => __('Edit building', MRED_TEXT_DOMAIN),
				'new_item' => __('Add a building', MRED_TEXT_DOMAIN),
				'search_items' => __('Find a building', MRED_TEXT_DOMAIN),
				'not_found' => __('No building found', MRED_TEXT_DOMAIN),
				'not_found_in_trash' => __('No building found in the trash', MRED_TEXT_DOMAIN),
			),
			'public' => true,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => array('title', 'editor')
		));


		if(function_exists("register_field_group"))
		{
			register_field_group(array (
				'id' => 'acf_building-details',
				'title' => 'Building details',
				'fields' => $this->getBuildingFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::CPT_BUILDING,
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}
	}

	public function getBuildingFieldDefinitions() {
		return array (
					array (
						'key' => 'field_537cc5f6f641d',
						'label' => 'Code',
						'name' => 'code',
						'type' => $this->getMultilingualFieldType('text'),
						'required' => 1,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_53bd10a7c489b',
						'label' => 'Building image',
						'name' => 'building_image',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_53bd10a7c489z',
						'label' => 'Building image 3D',
						'name' => 'building_image_3d',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_53bd2ef64700c',
						'label' => 'CSS top floor offset (%)',
						'name' => 'top_floor_offset',
						'type' => 'text',
						'instructions' => 'Numeric (can be decimal)',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_53bd2f154700d',
						'label' => 'CSS Floor height (%)',
						'name' => 'css_floor_height',
						'type' => 'text',
						'instructions' => 'Numeric (can be decimal)',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_53fb2826fc04d',
						'label' => 'Development plan coordinates',
						'name' => 'dev_plan_coordinates',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5418a78298667',
						'label' => 'Development plan hover image',
						'name' => 'dev_plan_hover_image',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
				);
	}

	private function createFloorCustomPostType() {
		$supports = array('title');

		// qTranslate won't fire up for other fields unless the editor is present
		if ($this->qtranslate_active) {
			$supports[] = 'editor';
		}

		register_post_type(self::CPT_FLOOR, array(
			'labels' => array(
				'name' => __('Floors', MRED_TEXT_DOMAIN),
				'singular_name' => __('Floor', MRED_TEXT_DOMAIN),
				'add_new_item' => __('Add a floor', MRED_TEXT_DOMAIN),
				'edit_item' => __('Edit floor', MRED_TEXT_DOMAIN),
				'new_item' => __('Add a floor', MRED_TEXT_DOMAIN),
				'search_items' => __('Find a floor', MRED_TEXT_DOMAIN),
				'not_found' => __('No floor found', MRED_TEXT_DOMAIN),
				'not_found_in_trash' => __('No floor found in the trash', MRED_TEXT_DOMAIN),
			),
			'public' => false,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => $supports
		));

		if(function_exists("register_field_group"))
		{
			register_field_group(array (
				'id' => 'acf_floor-details',
				'title' => 'Floor details',
				'fields' => $this->getFloorFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::CPT_FLOOR,
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}

	}

	public function getFloorFieldDefinitions() {
		return array (
					array (
						'key' => 'field_537cd163db6f9',
						'label' => 'Code',
						'name' => 'code',
						'type' => $this->getMultilingualFieldType('text'),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537cd16ddb6fa',
						'label' => 'Ordinal',
						'name' => 'ordinal',
						'type' => $this->getMultilingualFieldType('text'),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537cd17bdb6fb',
						'label' => 'Order',
						'name' => 'order',
						'type' => 'number',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5395c668b0fe1',
						'label' => 'Plan',
						'name' => 'plan',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_5409d54c4ee71',
						'label' => 'Development plan coordinates',
						'name' => 'dev_plan_coordinates',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5418a82228fa6',
						'label' => 'Development plan hover image',
						'name' => 'dev_plan_hover_image',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_54eb24bc34219',
						'label' => 'Walls Image',
						'name' => 'walls_image',
						'prefix' => '',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'medium',
						'library' => 'all',
					),
					array (
						'key' => 'field_54eb25b3cddf4',
						'label' => 'Floor overlay image',
						'name' => 'floor_overlay_image',
						'prefix' => '',
						'type' => 'image',
						'instructions' => 'Transparent PNG, the same size as the floor PNG (walls image)',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'medium',
						'library' => 'all',
					),
				);
	}

	private function createPlanCustomPostType() {
		$supports = array('title');

		// qTranslate won't fire up for other fields unless the editor is present
		if ($this->qtranslate_active) {
			$supports[] = 'editor';
		}

		register_post_type(self::CPT_PLAN, array(
			'labels' => array(
				'name' => __('Plans', MRED_TEXT_DOMAIN),
				'singular_name' => __('Plan', MRED_TEXT_DOMAIN),
				'add_new_item' => __('Add a plan', MRED_TEXT_DOMAIN),
				'edit_item' => __('Edit plan', MRED_TEXT_DOMAIN),
				'new_item' => __('Add a plan', MRED_TEXT_DOMAIN),
				'search_items' => __('Find a plan', MRED_TEXT_DOMAIN),
				'not_found' => __('No plan found', MRED_TEXT_DOMAIN),
				'not_found_in_trash' => __('No plan found in the trash', MRED_TEXT_DOMAIN),
			),
			'public' => false,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => $supports
		));

		if(function_exists("register_field_group"))
		{
			register_field_group(array (
				'id' => 'acf_plan-details',
				'title' => 'Plan details',
				'fields' => $this->getPlanFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::CPT_PLAN,
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}

	}

	public function getPlanFieldDefinitions() {
		return array (
					array (
						'key' => 'field_537db184db1ce',
						'label' => 'Code',
						'name' => 'code',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537db189db1cf',
						'label' => 'Detailed plan image (SVG)',
						'name' => 'detailed_plan_image',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_54e75a810ec2f',
						'label' => 'Detailed plan image fallback',
						'name' => 'detailed_plan_image_fallback',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_537db1b8db1d0',
						'label' => 'PDF',
						'name' => 'pdf',
						'type' => 'file',
						'save_format' => 'object',
						'library' => 'all',
					),
					array (
						'key' => 'field_53bbf7a6da5dc',
						'label' => 'SVG ID',
						'name' => 'svg_id',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_55896a273ba7d',
						'label' => '3D image',
						'name' => 'image_3d',
						'prefix' => '',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_555c37f15bb3e',
						'label' => 'Additional Images',
						'name' => 'additional_images',
						'prefix' => '',
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'min' => '',
						'max' => '',
						'layout' => 'table',
						'button_label' => 'Ajouter un élément',
						'sub_fields' => array (
							array (
								'key' => 'field_555c38105bb3f',
								'label' => 'Additional image',
								'name' => 'additional_image',
								'prefix' => '',
								'type' => 'image',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'return_format' => 'array',
								'preview_size' => 'medium',
								'library' => 'all',
							),
							array (
								'key' => 'field_555c3c59f4155',
								'label' => 'Additional image Fallback (for SVGs)',
								'name' => 'additional_image_fallback',
								'prefix' => '',
								'type' => 'image',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'return_format' => 'array',
								'preview_size' => 'medium',
								'library' => 'all',
							),
						),
					),
				);
	}

	private function createLotCustomPostType() {
		$supports = array('title');

		// qTranslate won't fire up for other fields unless the editor is present
		if ($this->qtranslate_active) {
			$supports[] = 'editor';
		}

		register_post_type(self::CPT_LOT, array(
			'labels' => array(
				'name' => __('Lots', MRED_TEXT_DOMAIN),
				'singular_name' => __('Lot', MRED_TEXT_DOMAIN),
				'add_new_item' => __('Add a lot', MRED_TEXT_DOMAIN),
				'edit_item' => __('Edit lot', MRED_TEXT_DOMAIN),
				'new_item' => __('Add a lot', MRED_TEXT_DOMAIN),
				'search_items' => __('Find a lot', MRED_TEXT_DOMAIN),
				'not_found' => __('No lot found', MRED_TEXT_DOMAIN),
				'not_found_in_trash' => __('No lot found in the trash', MRED_TEXT_DOMAIN),
			),
			'public' => true,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => $supports
		));

		if(function_exists("register_field_group"))
		{
			register_field_group(array (
				'id' => 'acf_lot-details',
				'title' => 'Lot details',
				'fields' => $this->getLotFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::CPT_LOT,
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}
	}

	public function getLotFieldDefinitions() {
		return array (
					array (
						'key' => 'field_537da1f32738f',
						'label' => 'Code',
						'name' => 'code',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537da1fd27390',
						'label' => 'Opus code',
						'name' => 'opus_code',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537da20727391',
						'label' => 'Rooms',
						'name' => 'pieces',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537da21727392',
						'label' => 'Surface interior',
						'name' => 'surface_interior',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537da22227393',
						'label' => 'Surface balcony',
						'name' => 'surface_balcony',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537da22d27394',
						'label' => 'Surface terrace',
						'name' => 'surface_terrace',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537da23727395',
						'label' => 'Surface garden',
						'name' => 'surface_garden',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537da24827396',
						'label' => 'Surface weighted',
						'name' => 'surface_weighted',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537da25227397',
						'label' => 'Availability',
						'name' => 'availability',
						'type' => 'radio',
						'choices' => array (
							'available' => 'Available',
							'reserved' => 'Reserved',
							'sold' => 'Sold',
						),
						'other_choice' => 0,
						'save_other_choice' => 0,
						'default_value' => '',
						'layout' => 'vertical',
					),
					array (
						'key' => 'field_53bce5f575b49',
						'label' => 'SVG ID',
						'name' => 'svg_id',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_53df4a7b6ab08',
						'label' => 'PDF',
						'name' => 'pdf',
						'type' => 'file',
						'save_format' => 'object',
						'library' => 'all',
					),
					array (
						'key' => 'field_53ef3ace5f393',
						'label' => 'Floor position image',
						'name' => 'floor_position_image',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_53ef433ab518b',
						'label' => 'Floor position image fallback',
						'name' => 'floor_position_image_fallback',
						'type' => 'image',
						'instructions' => 'For browsers that do not support SVG',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_54f55e45ce2f4',
						'label' => 'Lot situation image',
						'name' => 'lot_situation_image',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_54f55e81ce2f5',
						'label' => 'Lot situation image fallback',
						'name' => 'lot_situation_image_fallback',
						'type' => 'image',
						'instructions' => 'For browsers that do not support SVG',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_5524d33209c10',
						'label' => 'Price',
						'name' => 'price',
						'type' => 'text',
						'default_value' => '',
						'instructions' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					)
				);
	}

	private function createEntryCustomPostType() {
		$supports = array('title');

		// qTranslate won't fire up for other fields unless the editor is present
		if ($this->qtranslate_active) {
			$supports[] = 'editor';
		}

		register_post_type(self::CPT_ENTRY, array(
			'labels' => array(
				'name' => __('Entries', MRED_TEXT_DOMAIN),
				'singular_name' => __('Entry', MRED_TEXT_DOMAIN),
				'add_new_item' => __('Add an entry', MRED_TEXT_DOMAIN),
				'edit_item' => __('Edit entry', MRED_TEXT_DOMAIN),
				'new_item' => __('Add an entry', MRED_TEXT_DOMAIN),
				'search_items' => __('Find an entry', MRED_TEXT_DOMAIN),
				'not_found' => __('No entry found', MRED_TEXT_DOMAIN),
				'not_found_in_trash' => __('No entry found in the trash', MRED_TEXT_DOMAIN),
			),
			'public' => false,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => $supports
		));

		if(function_exists("register_field_group"))
		{
			register_field_group(array (
				'id' => 'acf_entry-details',
				'title' => 'Entry details',
				'fields' => $this->getEntryFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::CPT_ENTRY,
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}
	}

	public function getEntryFieldDefinitions() {
		return array (
					array (
						'key' => 'field_53d8f759aa361',
						'label' => 'Code',
						'name' => 'code',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
				);
	}

	private function createLotTypeTaxonomy() {
		$applies_to = array(
			self::CPT_BUILDING,
			self::CPT_LOT
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => array(
				'name'              => __( 'Lot types', MRED_TEXT_DOMAIN ),
				'singular_name'     => __( 'Lot type', MRED_TEXT_DOMAIN ),
				'search_items'      => __( 'Search lot types', MRED_TEXT_DOMAIN ),
				'all_items'         => __( 'All lot types', MRED_TEXT_DOMAIN ),
				'parent_item'       => __( 'Parent lot type', MRED_TEXT_DOMAIN ),
				'parent_item_colon' => __( 'Parent lot type:', MRED_TEXT_DOMAIN ),
				'edit_item'         => __( 'Edit lot type', MRED_TEXT_DOMAIN ),
				'update_item'       => __( 'Update lot type', MRED_TEXT_DOMAIN ),
				'add_new_item'      => __( 'Add New lot type', MRED_TEXT_DOMAIN ),
				'new_item_name'     => __( 'New lot type Name', MRED_TEXT_DOMAIN ),
				'menu_name'         => __( 'Lot types', MRED_TEXT_DOMAIN ),
			),
			'show_ui'           => true,
			'show_admin_column' => true,
		);


		register_taxonomy( self::TAX_LOT_TYPES, $applies_to, $args );
	}


	public function createRelationships() {
		if (! function_exists('p2p_register_connection_type')) {
			error_log("p2p_register_connection_type function doesn't exist");
			return;
		}

		$relationships = array(
			self::REL_SECTOR_DEV => array(
				'from' => self::CPT_SECTOR,
				'to' => self::CPT_DEVELOPMENT
			),
			self::REL_BUILDING_SECTOR => array(
				'from' => self::CPT_BUILDING,
				'to' => self::CPT_SECTOR
			),
			self::REL_FLOOR_BUILDING => array(
				'from' => self::CPT_FLOOR,
				'to' => self::CPT_BUILDING
			),
			self::REL_LOT_FLOOR => array(
				'from' => self::CPT_LOT,
				'to' => self::CPT_FLOOR,
				'cardinality' => 'many-to-many',
				'fields' => array(
					'entry' => array(
						'title' => 'Entry floor',
						'type' => 'checkbox'
					),
					'svg_id' => array(
						'title' => 'SVG ID',
						'type' => 'text',
					),
					'floor_synoptic_coordinates' => array( // App specific, but it'd be tricky to add it in the MEO Vision App Data plugin
						'title' => 'Floor synoptic coordinates',
						'type' => 'text',
					)
				),
				'sortable' => 'any'
			),
			self::REL_LOT_PLAN => array(
				'from' => self::CPT_LOT,
				'to' => self::CPT_PLAN
			),
			self::REL_LOT_ENTRY => array(
				'from' => self::CPT_LOT,
				'to' => self::CPT_ENTRY
			),
			self::REL_ENTRY_BUILDING => array(
				'from' => self::CPT_ENTRY,
				'to' => self::CPT_BUILDING
			)
		);

		foreach ($relationships as $relationship_name => $relationship_details) {
			$args =  array(
				'name' => $relationship_name,
				'admin_column' => 'from',
				'cardinality' => 'many-to-one'
			);
			foreach ($relationship_details as $relationship_field_name => $relationship_field_value) {
				$args[$relationship_field_name] = $relationship_field_value;
			}
			p2p_register_connection_type($args);
		}
	}

	private function sanitiseIds($ids) {
		$result = array();
		if (!empty($ids)) {
			foreach ($ids as $id) {
				$checked_id = (int) $id;
				if ($checked_id) {
					$result[] = $checked_id;
				}
			}
		}
		return $result;
	}

	private function getCachedValues($cache, $ids) {
		$result = null;
		if (!empty($cache)) {
			$result = array();
			foreach ($ids as $id) {
				if (array_key_exists($id, $cache)) {
					$result[$id] = $cache[$id];
				}
			}
		}
		return $result;
	}

	private function getTypeDetails($post_type, $ids, $field_definitions, $relationships, $order_meta_field = 'code') {
		global $wpdb;

		// These field types are complex, so we'll fall back to get_field()
		$unhandlable_types = array(
			'repeater'
		);

		$fields = array(
			"p.id as id",
			"p.post_title as name",
			"p.post_name as slug"
		);

		$joins = array(
			"{$wpdb->posts} p"
		);

		$conditions = array(
			"p.post_type = '{$post_type}'",
			"p.post_status = 'publish'"
		);

		$order = 'p.post_title asc';

		if (!empty($ids)) {
			$conditions[] = 'p.id in (' . implode(', ', $ids) . ')';
		}

		$next_table_index = 1;

		foreach ($field_definitions as $field_definition) {
			if (in_array($field_definition['type'], $unhandlable_types)) {
				// Don't bother retrieving these - we'll have to use get_field() anyway
				continue;
			}

			$fields[] = "nullif(t{$next_table_index}.meta_value, '') as `" . $field_definition['name'] . "`";
			$joins[] = "left join {$wpdb->postmeta} t{$next_table_index} on p.id = t{$next_table_index}.post_id and t{$next_table_index}.meta_key = '" . $field_definition['name'] . "'";

			if ($field_definition['name'] == $order_meta_field) {
				$order = "t{$next_table_index}.meta_value asc";
			}

			$next_table_index++;
		}

		foreach ($relationships as $relationship_name => $relationship_identity) {
			$fields[] = "t{$next_table_index}.p2p_to as {$relationship_name}_id";
			$joins[] = "left join {$wpdb->prefix}p2p t{$next_table_index} on p.id = t{$next_table_index}.p2p_from and t{$next_table_index}.p2p_type = '{$relationship_identity}'";

			$next_table_index++;
		}

		$sql = "select " . join(", \n ", $fields ) . " \nfrom " . join(" \n ", $joins) . " \n where " . join(" \n and ", $conditions ) . " \n order by " . $order;

		$rows = $wpdb->get_results($sql);

		foreach ($rows as $row) {
			$obj = array(
				'id'   => $row->id,
				'name' => __($row->name),
				'slug' => __($row->slug),
				'url'  => get_permalink($row->id)
			);

			foreach ($field_definitions as $field_definition) {
				if (in_array($field_definition['type'], $unhandlable_types)) {
					$obj[$field_definition['name']] = get_field($field_definition['name'], $row->id);
					continue;
				}

				$type = preg_replace('/^qtranslate_/', '', $field_definition['type']);
				$value = __($row->{$field_definition['name']});

				if ($type == 'image') {
					$obj[$field_definition['name']] = $this->extractMediaDetails($value);
				}
				elseif ($type == 'file') {
					$obj[$field_definition['name']] = $this->extractFileDetails($value);
				}
				else {
					$obj[$field_definition['name']] = $value;
				}
			}

			foreach ($relationships as $relationship_name => $relationship_identity) {
				if (!empty($row->{"{$relationship_name}_id"})) {
					$obj["{$relationship_name}_id"] = $row->{"{$relationship_name}_id"};
				}
			}

			$result[$row->id] = $obj;
		}

		return $result;
	}

	private function extractMediaDetails($id, $size = 'full') {
		$result = null;
		$details = wp_get_attachment_image_src( $id, $size );

		if ($details) {
			$result = array(
				'id'        => $id,
				'url'       => $details[0],
				'mime_type' => get_post_mime_type( $id ),
				'width'     => $details[1],
				'height'    => $details[2]
			);
		}

		return $result;
	}

	private function extractFileDetails($id) {
		$result = null;
		$url = wp_get_attachment_url( $id );

		if ($url) {
			$result = array(
				'id'        => $id,
				'url'       => $url,
				'mime_type' => get_post_mime_type( $id )
			);
		}

		return $result;
	}
}
