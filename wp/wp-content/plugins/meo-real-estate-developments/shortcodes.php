<?php

/* ----------------------------------------------------------------------- */
if (function_exists('add_shortcode')) {
	add_shortcode( 'lot_list' , 'mred_lot_list' );
}
function mred_lot_list() {
	ob_start();
	?>
	<div id="apartment-list">
		<ul class="apartment-list">
			<?php
			$lots = mred_get_lots();
			$floors = mred_get_floors();
			$buildings = mred_get_buildings();

			$odd = true;
			$visibility_class = 'first-visible';
			foreach ($lots as $lot) {
				$rowtype = ( $odd ? 'odd' : 'even' );
				$odd = !$odd;

				$floor = $floors[$lot['floor_id']];
				$floor_link = $buildings[$floor['building_id']]['url'];

				echo mred_get_lot_markup_list ($lot, $floor_link, array($rowtype, $visibility_class));
				$visibility_class = '';
			} ?>
		</ul>
		<div class="clear"></div>
	</div>
	<?php

	$result = ob_get_clean();

	return $result;
}

/* ----------------------------------------------------------------------- */

if (function_exists('add_shortcode')) {
	add_shortcode('lot_list_plan', 'mred_lot_list_plan');
}
function mred_lot_list_plan() {
	$lots = mred_get_lots();
	$floors = mred_get_floors();
	$buildings = mred_get_buildings();

	ob_start();
	?>
	<div id="apartment-images" class="columns">
		<ul class="apartment-list vc_row-fluid <?php echo join(" ", mred_get_lot_thumb_wrapper_classes()); ?>">
			<?php
			$odd = true;
			$lot_index = 0;
			foreach ($lots as $lot) {
				$rowtype = ( $odd ? 'odd' : 'even' );
				$odd = !$odd;

				$floor = $floors[$lot['floor_id']];
				$floor_link = $buildings[$floor['building_id']]['url'];

				echo mred_get_lot_markup_plan ($lot, $floor_link, array(
					$rowtype,
					'column-' . ( $lot_index % 3 + 1 ) . '-3',
					'column-' . ( $lot_index % 4 + 1 ) . '-4'
				));
				$lot_index++;
			} ?>

		</ul>
		<div class="clear"></div>
	</div>
	<?php

	$result = ob_get_clean();

	return $result;
}

/* ----------------------------------------------------------------------- */

function mred_get_svg_path_points($path, $behaviour_on_error = null) {
	preg_match_all('/ *([A-Za-z][-0-9,\.]+)/', $path, $matches);
	$last_point = null;
	$first_point = null;

	$result = array();
	foreach ($matches[1] as $instruction) {
		$point = array();
		$code = $instruction[0];
		$value = substr($instruction, 1);
		switch ($code) {
			case 'Z':
			case 'z':
				$point = $first_point;
				break;

			case 'M':
			case 'L':
				// Absolute
				$coordinates = preg_split('/,/', $value);
				$point = array(
					'x' => (float) $coordinates[0],
					'y' => (float) $coordinates[1]
				);
				break;

			case 'm':
			case 'l':
				// Relative
				$coordinates = preg_split('/,/', $value);
				$point = array(
					'x' => (float) $coordinates[0] + $last_point['x'],
					'y' => (float) $coordinates[1] + $last_point['y']
				);
				break;

			case 'H':
				// Absolute
				$point = array(
					'x' => (float) $value,
					'y' => $last_point['y']
				);
				break;

			case 'h':
				// Relative
				$point = array(
					'x' => (float) $value + $last_point['x'],
					'y' => $last_point['y']
				);
				break;

			case 'V':
				// Absolute
				$point = array(
					'x' => $last_point['x'],
					'y' => (float) $value
				);
				break;

			case 'v':
				// Relative
				$point = array(
					'x' => $last_point['x'],
					'y' => (float) $value + $last_point['y']
				);
				break;



			default:
				$error_message = "Unhandled code $code (probably a curved line) in mred_get_svg_path_points() - $path";
				@error_log($error_message);
				if ($behaviour_on_error == "die") {
					die($error_message);
				}
				elseif ($behaviour_on_error != null) {
					print $error_message . "\n";
				}
				continue;
		}

		$result[] = $point;
		if (empty($first_point)) {
			$first_point = $point;
		}
		$last_point = $point;
	}
	return $result;
}

function mred_get_svg_rect_points($svg_element) {
	$x = (float) $svg_element->getAttribute('x');
	$y = (float) $svg_element->getAttribute('y');
	$w = (float) $svg_element->getAttribute('width');
	$h = (float) $svg_element->getAttribute('height');

	return array(
		array( 'x' => $x,      'y' => $y      ),
		array( 'x' => $x + $w, 'y' => $y      ),
		array( 'x' => $x + $w, 'y' => $y + $h ),
		array( 'x' => $x,      'y' => $y + $h )
	);
}

function mred_get_svg_element_points($svg_element, $behaviour_on_error = null) {
	$points = array();

	$raw_points = $svg_element->getAttribute('points');
	if (!empty($raw_points)) {
		$point_pairs = preg_split('/ +/', $raw_points, -1, PREG_SPLIT_NO_EMPTY);
		foreach ($point_pairs as $point_pair) {
			$coordinates = preg_split('/,/', $point_pair);
			$points[] = array(
				'x' => (float) $coordinates[0],
				'y' => (float) $coordinates[1]
			);
		}
	}
	elseif ($svg_element->getAttribute('x')) {
		$points = mred_get_svg_rect_points($svg_element);
	}
	else {
		$points = mred_get_svg_path_points($svg_element->getAttribute('d'), $behaviour_on_error);
	}

	return $points;
}

function mred_get_points_extent($points) {
	$min_x = 999999999;
	$max_x = 0;
	$min_y = 999999999;
	$max_y = 0;

	foreach ($points as $point) {
		if ($point['x'] < $min_x) {
			$min_x = $point['x'];
		}
		if ($point['y'] < $min_y) {
			$min_y = $point['y'];
		}
		if ($point['x'] > $max_x) {
			$max_x = $point['x'];
		}
		if ($point['y'] > $max_y) {
			$max_y = $point['y'];
		}
	}

	return array(
		'min_x' => $min_x,
		'max_x' => $max_x,
		'min_y' => $min_y,
		'max_y' => $max_y
	);
}

function mred_get_svg_element_centre($svg_element) {
	$points = mred_get_svg_element_points($svg_element);

	$extent = mred_get_points_extent($points);

	$centre_x = $extent['min_x'] + ($extent['max_x'] - $extent['min_x']) / 2;
	$centre_y = $extent['min_y'] + ($extent['max_y'] - $extent['min_y']) / 2;

	return array(
		'x' => $centre_x,
		'y' => $centre_y
	);
}

function mred_get_floor_svg($floor, $highlight_lot_id = null, $strip_fills = true ) {
	$upload_directory = wp_upload_dir();
	$cache_dir = $upload_directory['basedir'] . '/cache/svg';
	$cache_file = $cache_dir . '/' . $floor['id'] . '.svg';

	wp_mkdir_p($cache_dir);
	if (file_exists($cache_file)) {
		echo file_get_contents($cache_file);
		return;
	}

	require_once "classes/svglib/svglib.php";
	$file = WP_CONTENT_DIR . '/uploads/' . get_post_meta( $floor['plan']['id'], '_wp_attached_file', true );

	$floor_plan = SVGDocument::getInstance( $file );

	$font_size = 17; // Needed to determine size of label background.  Should match the CSS
	if (!$strip_fills) {
		// Add CSS for the backgrounds of the labels
		$floor_plan->prependChild('style', "rect.label_background { fill: #ffffff; } text { font-size: " . $font_size . "px; text-anchor: middle; alignment-baseline: middle; text-transform: lowercase; }" );
	}

	$lots = mred_get_lots();
	$plans = mred_get_plans();
	foreach ($lots as $lot) {
		if ($lot['floor_id'] != $floor['id']) {
			continue;
		}
		$plan = $plans[$lot['plan_id']];
		if (empty($plan)) {
			continue;
		}

		$svg_id = ( isset($lot['svg_id']) && !empty($lot['svg_id']) ) ? $lot['svg_id'] : $plan['svg_id'];
		if (empty($svg_id)) {
			continue;
		}

		$svg_element = $floor_plan->getElementById( $svg_id );
		if (!is_a($svg_element, 'SVGDocument')) {
			continue;
		}

		$class = 'svg_lot';
		if ($highlight_lot_id) {
			if ($highlight_lot_id == $lot['id']) {
				$class .= ' highlight';
			}
		}
		else {
			$svg_element->setAttribute("onclick", "loadLot(" . $lot['id'] . ");");

			// Make sure don't have hardcoded colours
			if ($strip_fills) {
				$svg_element->removeAttribute('fill');
			}

			// Set the classes
			$class .= ' ' . mred_get_availability_class($lot['availability']);

			$centre = mred_get_svg_element_centre($svg_element);

			$label_group = SVGGroup::getInstance('lot_status_' . $lot['id'] . '_group');
			$label_group->setAttribute('class', 'label_background_group');

			$label_background = SVGRect::getInstance($centre['x'] - (3 * $font_size), $centre['y'] - $font_size, 'lot_status_bg_' . $lot['id'], 6 * $font_size, intval(1.5 * $font_size) );
			$label_background->setAttribute('class', 'label_background');

			$label_text = mred_get_availability_description($lot['availability']);
			$label_text = mb_htmlentities($label_text);

			$label = SVGText::getInstance($centre['x'], $centre['y'], 'lot_status_' . $lot['id'], $label_text );

			$label_group->append($label_background);
			$label_group->append($label);

			$floor_plan->append($label_group);
		}
		$svg_element->setAttribute('class', $class);

	}

	$result = $floor_plan->asXML(null, false);

	// Save the result
	$can_cache = true;
	$fh = fopen($cache_file, 'w') or $can_cache = false;
	if ($can_cache) {
		fwrite($fh, $result);
		fclose($fh);
	}

	echo $result;
}

if (!function_exists('mb_htmlentities')) {

	class MbHtmlentitiesCallback {
		private $hex;

		function __construct($hex) {
			$this->hex = $hex;
		}

		public function callback($match) {
			return sprintf($hex ? '&#x%X;' : '&#%d;', mb_ord($match[0]));
		}

	}

	function mb_htmlentities($string, $hex = true) {
		$callback = new MbHtmlentitiesCallback($hex);
		return preg_replace_callback('/[\x{80}-\x{10FFFF}]/u', array($callback, 'callback'), $string);
	}
}

if (!function_exists('mb_ord')) {

    function mb_ord($char, $encoding = 'UTF-8') {
        if ($encoding === 'UCS-4BE') {
            list(, $ord) = (strlen($char) === 4) ? @unpack('N', $char) : @unpack('n', $char);
            return $ord;
        } else {
            return mb_ord(mb_convert_encoding($char, 'UCS-4BE', $encoding), 'UCS-4BE');
        }
    }

}
/* ----------------------------------------------------------------------- */
