msgid ""
msgstr ""
"Project-Id-Version: MEO real estate\n"
"POT-Creation-Date: 2015-05-18 13:40+0100\n"
"PO-Revision-Date: 2015-05-18 20:49+0100\n"
"Last-Translator: Peter Holberton <webdev@allmeo.com>\n"
"Language-Team: MEO <webdev@allmeo.com>\n"
"Language: fr_FR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.7.4\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;mred_translate\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../api.php:129
msgid "Available"
msgstr "Libre"

#: ../api.php:130
msgid "Reserved"
msgstr "Réservé"

#: ../api.php:131
msgid "Sold"
msgstr "Vendu"

#: ../classes/class-development-dao-p2p.php:476
msgid "Developments"
msgstr "Développements"

#: ../classes/class-development-dao-p2p.php:477
msgid "Development"
msgstr "Développement"

#: ../classes/class-development-dao-p2p.php:478
#: ../classes/class-development-dao-p2p.php:480
msgid "Add a development"
msgstr "Ajouter un développement"

#: ../classes/class-development-dao-p2p.php:479
msgid "Edit development"
msgstr "Modifier développement"

#: ../classes/class-development-dao-p2p.php:481
msgid "Find a development"
msgstr "Trouver un développement"

#: ../classes/class-development-dao-p2p.php:482
msgid "No development found"
msgstr "Aucun développement trouvée"

#: ../classes/class-development-dao-p2p.php:483
msgid "No development found in the trash"
msgstr "Aucun développement trouvée dans la poubelle"

#: ../classes/class-development-dao-p2p.php:567
msgid "Sectors"
msgstr ""

#: ../classes/class-development-dao-p2p.php:568
msgid "Sector"
msgstr ""

#: ../classes/class-development-dao-p2p.php:569
#: ../classes/class-development-dao-p2p.php:571
msgid "Add an sector"
msgstr ""

#: ../classes/class-development-dao-p2p.php:570
msgid "Edit sector"
msgstr ""

#: ../classes/class-development-dao-p2p.php:572
msgid "Find an sector"
msgstr ""

#: ../classes/class-development-dao-p2p.php:573
msgid "No sector found"
msgstr ""

#: ../classes/class-development-dao-p2p.php:574
msgid "No sector found in the trash"
msgstr ""

#: ../classes/class-development-dao-p2p.php:643
msgid "Buildings"
msgstr "Bâtiments"

#: ../classes/class-development-dao-p2p.php:644 ../mred-pluggable.php:240
msgid "Building"
msgstr "Bâtiment"

#: ../classes/class-development-dao-p2p.php:645
#: ../classes/class-development-dao-p2p.php:647
msgid "Add a building"
msgstr "Ajouter un bâtiment"

#: ../classes/class-development-dao-p2p.php:646
msgid "Edit building"
msgstr "Modifier bâtiment"

#: ../classes/class-development-dao-p2p.php:648
msgid "Find a building"
msgstr "Trouver un bâtiment"

#: ../classes/class-development-dao-p2p.php:649
msgid "No building found"
msgstr "Aucun bâtiment trouvé"

#: ../classes/class-development-dao-p2p.php:650
msgid "No building found in the trash"
msgstr "Aucun bâtiment trouvée dans la poubelle"

#: ../classes/class-development-dao-p2p.php:774
msgid "Floors"
msgstr "Étages"

#: ../classes/class-development-dao-p2p.php:775 ../mred-pluggable.php:245
msgid "Floor"
msgstr "Étage"

#: ../classes/class-development-dao-p2p.php:776
#: ../classes/class-development-dao-p2p.php:778
msgid "Add a floor"
msgstr "Ajouter un étage"

#: ../classes/class-development-dao-p2p.php:777
msgid "Edit floor"
msgstr "Modifier étage"

#: ../classes/class-development-dao-p2p.php:779
msgid "Find a floor"
msgstr "Trouver un étage"

#: ../classes/class-development-dao-p2p.php:780
msgid "No floor found"
msgstr "Aucun étage trouvée"

#: ../classes/class-development-dao-p2p.php:781
msgid "No floor found in the trash"
msgstr "Aucun étage trouvée dans la poubelle"

#: ../classes/class-development-dao-p2p.php:939
msgid "Plans"
msgstr "Plans"

#: ../classes/class-development-dao-p2p.php:940
msgid "Plan"
msgstr "Plan"

#: ../classes/class-development-dao-p2p.php:941
#: ../classes/class-development-dao-p2p.php:943
msgid "Add a plan"
msgstr "Ajouter un plan"

#: ../classes/class-development-dao-p2p.php:942
msgid "Edit plan"
msgstr "Modifier plan"

#: ../classes/class-development-dao-p2p.php:944
msgid "Find a plan"
msgstr "Trouver un plan"

#: ../classes/class-development-dao-p2p.php:945
msgid "No plan found"
msgstr "Aucun plan trouvée"

#: ../classes/class-development-dao-p2p.php:946
msgid "No plan found in the trash"
msgstr "Aucun plan trouvée dans la poubelle"

#: ../classes/class-development-dao-p2p.php:1051
msgid "Lots"
msgstr "Lots"

#: ../classes/class-development-dao-p2p.php:1052
msgid "Lot"
msgstr "Lot"

#: ../classes/class-development-dao-p2p.php:1053
#: ../classes/class-development-dao-p2p.php:1055
msgid "Add a lot"
msgstr "Ajouter un lot"

#: ../classes/class-development-dao-p2p.php:1054
msgid "Edit lot"
msgstr "Modifier lot"

#: ../classes/class-development-dao-p2p.php:1056
msgid "Find a lot"
msgstr "Trouver un lot"

#: ../classes/class-development-dao-p2p.php:1057
msgid "No lot found"
msgstr "Aucun lot trouvée"

#: ../classes/class-development-dao-p2p.php:1058
msgid "No lot found in the trash"
msgstr "Aucun lot trouvée dans la poubelle"

#: ../classes/class-development-dao-p2p.php:1294
msgid "Entries"
msgstr ""

#: ../classes/class-development-dao-p2p.php:1295 ../mred-pluggable.php:250
msgid "Entry"
msgstr ""

#: ../classes/class-development-dao-p2p.php:1296
#: ../classes/class-development-dao-p2p.php:1298
msgid "Add an entry"
msgstr ""

#: ../classes/class-development-dao-p2p.php:1297
msgid "Edit entry"
msgstr ""

#: ../classes/class-development-dao-p2p.php:1299
msgid "Find an entry"
msgstr ""

#: ../classes/class-development-dao-p2p.php:1300
msgid "No entry found"
msgstr ""

#: ../classes/class-development-dao-p2p.php:1301
msgid "No entry found in the trash"
msgstr ""

#: ../classes/class-development-dao-p2p.php:1366
#: ../classes/class-development-dao-p2p.php:1376
msgid "Lot types"
msgstr ""

#: ../classes/class-development-dao-p2p.php:1367
msgid "Lot type"
msgstr ""

#: ../classes/class-development-dao-p2p.php:1368
msgid "Search lot types"
msgstr ""

#: ../classes/class-development-dao-p2p.php:1369
msgid "All lot types"
msgstr ""

#: ../classes/class-development-dao-p2p.php:1370
msgid "Parent lot type"
msgstr ""

#: ../classes/class-development-dao-p2p.php:1371
msgid "Parent lot type:"
msgstr ""

#: ../classes/class-development-dao-p2p.php:1372
msgid "Edit lot type"
msgstr ""

#: ../classes/class-development-dao-p2p.php:1373
msgid "Update lot type"
msgstr ""

#: ../classes/class-development-dao-p2p.php:1374
msgid "Add New lot type"
msgstr ""

#: ../classes/class-development-dao-p2p.php:1375
msgid "New lot type Name"
msgstr ""

#: ../classes/class-meo-smart-capture-api.php:237
#: ../meo-real-estate-developments.php:114 ../mred-pluggable.php:255
msgid "Rooms"
msgstr "Pièces"

#: ../classes/class-meo-smart-capture-api.php:238
#: ../meo-real-estate-developments.php:115
msgid "Apartment"
msgstr "Appartement"

#: ../classes/class-meo-smart-capture-api.php:266
#: ../classes/class-meo-smart-capture-api.php:312
msgid "Invalid lot ID"
msgstr ""

#: ../classes/class-meo-smart-capture-api.php:275
msgid "Invalid status"
msgstr ""

#: ../classes/class-meo-smart-capture-api.php:284
msgid "Status updated"
msgstr ""

#: ../classes/class-meo-smart-capture-api.php:323
msgid "Price updated"
msgstr ""

#: ../meo-real-estate-developments.php:32
msgid ""
"Error: plugin \"MEO Real Estate Developments\" requires a newer version of "
"PHP to be running."
msgstr ""
"Erreur: extension \"MEO Real Estate Developments\" nécessite une version "
"plus récente de PHP."

#: ../meo-real-estate-developments.php:33
msgid "Minimal version of PHP required: "
msgstr "Version minimale de PHP requise:"

#: ../meo-real-estate-developments.php:34
msgid "Your server's PHP version: "
msgstr "La version de PHP sur votre serveur:"

#: ../meo-real-estate-developments.php:49
msgid ""
"Error: plugin \"MEO Real Estate Developments\" depends on the following\" "
"plugins.  "
msgstr ""
"Erreur: extension \"MEO Real Estate Developments\" dépend des plugins "
"suivants."

#: ../meo-real-estate-developments.php:50
msgid "Please install and/or activate them"
msgstr "Merci de les installer et/ou activer"

#: ../mred-pluggable.php:219 ../mred-pluggable.php:352
#: ../mred-pluggable.php:400
msgid "p."
msgstr "p."

#: ../mred-pluggable.php:225
msgid "All"
msgstr "Tous"

#: ../mred-pluggable.php:230
msgid "Type"
msgstr ""

#: ../mred-pluggable.php:235
msgid "sector"
msgstr ""

#: ../mred-pluggable.php:260
msgid "Balcony"
msgstr "Balcon"

#: ../mred-pluggable.php:263
msgid "No"
msgstr "Non"

#: ../mred-pluggable.php:264
msgid "Yes"
msgstr "Oui"

#: ../mred-pluggable.php:268
msgid "Availability"
msgstr "Disponibilité"

#: ../mred-pluggable.php:403
msgid "Info pack"
msgstr "Dossier complet"

#: ../mred-pluggable.php:405
msgid "View lot details"
msgstr "Voir ce lot"

#: ../mred-pluggable.php:443
msgid "Download Info pack"
msgstr "Télécharger dossier complet"

#: ../mred-pluggable.php:715
msgid "p"
msgstr ""

#~ msgid "Bld"
#~ msgstr "Bât"

#~ msgid "Error"
#~ msgstr "Erreur"

#~ msgid "Invalid email address or download code"
#~ msgstr "Adresse mail ou un code de téléchargement invalide"

#~ msgid "Invalid lot"
#~ msgstr "Lot invalid"

#~ msgid "Missing or invalid plan details for lot"
#~ msgstr "Plan manquant ou invalide pour le lot"

#~ msgid "Filter by"
#~ msgstr "Filtrer par"

#~ msgid "Pages:"
#~ msgstr "Pages:"

#~ msgid "Edit Page"
#~ msgstr "Modifier page"

#~ msgid "Dimensions"
#~ msgstr "Dimensions"

#~ msgid "Terrace area"
#~ msgstr "Surface terrasse"

#~ msgid "Balcony area"
#~ msgstr "Surface balcon"

#~ msgid "Garden area"
#~ msgstr "Surface jardin"

#~ msgid "Surface area"
#~ msgstr "Surface pondérée"

#~ msgid "Situation"
#~ msgstr "Situation"

#~ msgid "Download info pack"
#~ msgstr "Télécharger dossier complet"
