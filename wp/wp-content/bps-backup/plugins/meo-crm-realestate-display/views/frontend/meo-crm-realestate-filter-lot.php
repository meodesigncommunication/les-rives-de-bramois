<?php
/* 
 * 
 */
global $wpdb;
$upload_path = wp_upload_dir();
$detect = new Mobile_Detect();
$filters = $_SESSION['filters'];
$floors = RealestateModel::selectFloor(true);
$rooms = RealestateModel::selectLotRooms();
$buildings = RealestateModel::selectBuildingsWithLot();
$metas = RealestateModel::selectMeta();
$filter_datas = RealestateModel::selectFilterMetaValue();

$data = array();
$data = Timber::get_context();
$post = new TimberPost();
$data['post'] = $post;
$data['posts'] = Timber::get_posts();
$data['page'] = '[:fr]Liste des lots[:en]Lot list[:]';
$data['plugin_path'] = plugins_url();
$data['type_lot'] = '[:fr]Appartements[:en]Apartements[:]';
$data['metas'] = $metas;
$data['floors'] = $floors;
$data['rooms'] = $rooms;
$data['mobile_device'] = ($detect->isMobile() && !$detect->isTablet()) ? true : false ;
$data['ajaxurl'] = admin_url('admin-ajax.php');
$data['buildings'] = $buildings['buildings'];
$data['filters'] = $filters;
$data['filter_datas'] = $filter_datas;
$data['base_upload_url'] = $upload_path['baseurl'].'/';
$data['base_upload_dir'] = $upload_path['basedir'].'/';
$data['template_path'] = get_template_directory_uri();
$data['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));
$data['page_list_lot'] = site_url('/list-lots-building/');

Timber::render('twig/meo-crm-realestate-filter-list-lots.html.twig', $data);