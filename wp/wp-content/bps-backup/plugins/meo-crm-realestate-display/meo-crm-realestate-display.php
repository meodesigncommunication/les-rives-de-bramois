<?php 
/*
Plugin Name: MEO CRM Realestate Display
Description: Plugin permettant l'affichage le quartier des
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sarl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

define('MEO_CRM_REALESTATE_DISPLAY_ROOT', plugin_dir_path( __FILE__ ));
define('MEO_REALESTATE_SLUG', 'meo-crm-realestate-display');
define('MEO_REALESTATE_SLUG_DEVELOPPEMENT', 'meo-crm-realestate-developpement');
define('MEO_REALESTATE_SLUG_SECTOR', 'meo-crm-realestate-sector');
define('MEO_REALESTATE_SLUG_BUILDING', 'meo-crm-realestate-building');
define('MEO_REALESTATE_SLUG_LIST_LOTS', 'meo-crm-realestate-list-lots');
define('MEO_REALESTATE_SLUG_LOT', 'meo-crm-realestate-lot');

define('MEO_CRM_REALESTATE_DISPLAY_TPL_FRONT_DEVELOPPEMENT', '../../'.MEO_REALESTATE_SLUG.'/views/frontend/meo-crm-realestate-developpement.php');
define('MEO_CRM_REALESTATE_DISPLAY_TPL_FRONT_BUILDING', '../../'.MEO_REALESTATE_SLUG.'/views/frontend/meo-crm-realestate-building.php');
define('MEO_CRM_REALESTATE_DISPLAY_TPL_FRONT_LIST_LOTS', '../../'.MEO_REALESTATE_SLUG.'/views/frontend/meo-crm-realestate-list-lots.php');
define('MEO_CRM_REALESTATE_DISPLAY_TPL_FRONT_SECTOR', '../../'.MEO_REALESTATE_SLUG.'/views/frontend/meo-crm-realestate-sector.php');
define('MEO_CRM_REALESTATE_DISPLAY_TPL_FRONT_LOT', '../../'.MEO_REALESTATE_SLUG.'/views/frontend/meo-crm-realestate-lot.php');

$plugin_root = plugin_dir_path( __FILE__ );

require_once($plugin_root.'/ajax/functions.php');
require_once($plugin_root.'/models/realestateModel.php');

function meo_crm_realestate_display_activate()
{
    do_action('plugins_meo_crm_active_create_page');
}
register_activation_hook( __FILE__, 'meo_crm_realestate_display_activate' );



add_action('wp_ajax_get_development', 'get_development');
add_action('wp_ajax_nopriv_get_development', 'get_development');
function get_development() {

    // Init variables
    $building = $_GET['building'];
    $lot = $_GET['lot'];

    $development = RealestateModel::get_development(array('building' => $building, 'lot' => $lot));   
    
    // Create JSON page
    if(get_option('api_key') == $_GET['api_key'])
    {        
        header('Content-Type: application/json');
        echo $development;    
    }else{
        header('Content-Type: application/json');
        echo '[error]'; 
    }
    
    die();
}


#########
# FRONT #
#########

# PAGE CREATION

# Register Plugin templates

add_filter('meo_crm_core_templates_collector', 'meo_crm_realestate_templates_register', 1, 1);

function meo_crm_realestate_templates_register($pluginsTemplates){

    $pluginsTemplates[MEO_CRM_REALESTATE_DISPLAY_TPL_FRONT_DEVELOPPEMENT]   = 'MEO CRM REALESTATE Developpement';
    $pluginsTemplates[MEO_CRM_REALESTATE_DISPLAY_TPL_FRONT_BUILDING] = 'MEO CRM REALESTATE Building';
    $pluginsTemplates[MEO_CRM_REALESTATE_DISPLAY_TPL_FRONT_LIST_LOTS] = 'MEO CRM REALESTATE List Lots';    
    $pluginsTemplates[MEO_CRM_REALESTATE_DISPLAY_TPL_FRONT_SECTOR] = 'MEO CRM REALESTATE Sector';
    $pluginsTemplates[MEO_CRM_REALESTATE_DISPLAY_TPL_FRONT_LOT] = 'MEO CRM REALESTATE Lot';

    return $pluginsTemplates;
}

# Register Plugin Pages

add_filter( 'meo_crm_core_front_pages_collector', 'meo_crm_realestate_pages_register' );
function meo_crm_realestate_pages_register($pluginsPages) {            


    $pluginsPages[MEO_REALESTATE_SLUG_DEVELOPPEMENT] = array(
        'post_title' => 'MEO CRM REALESTATE Developpement',
        'post_content' => 'Page to be used for show a developpement',
        '_wp_page_template' => MEO_CRM_REALESTATE_DISPLAY_TPL_FRONT_DEVELOPPEMENT
    );

    $pluginsPages[MEO_REALESTATE_SLUG_BUILDING] = array(
        'post_title' => 'MEO CRM REALESTATE Building',
        'post_content' => 'Page to be used for show a developpement',
        '_wp_page_template' => MEO_CRM_REALESTATE_DISPLAY_TPL_FRONT_BUILDING
    );

    $pluginsPages[MEO_REALESTATE_SLUG_LIST_LOTS] = array(
        'post_title' => 'MEO CRM REALESTATE List Lots',
        'post_content' => 'Page to be used for show a developpement',
        '_wp_page_template' => MEO_CRM_REALESTATE_DISPLAY_TPL_FRONT_LIST_LOTS
    );

    $pluginsPages[MEO_REALESTATE_SLUG_SECTOR] = array(
        'post_title' => 'MEO CRM REALESTATE Sector',
        'post_content' => 'Page to be used for show a developpement',
        '_wp_page_template' => MEO_CRM_REALESTATE_DISPLAY_TPL_FRONT_SECTOR
    );
    
    $pluginsPages[MEO_REALESTATE_SLUG_LOT] = array(
        'post_title' => 'MEO CRM REALESTATE Lot',
        'post_content' => 'Page to be used for show a lot details',
        '_wp_page_template' => MEO_CRM_REALESTATE_DISPLAY_TPL_FRONT_LOT
    );

    # Create Pages for Plugins
    if(is_array($pluginsPages)){

            foreach($pluginsPages as $pluginSlug => $pluginPage){				
                if(!meo_crm_core_slug_exists($pluginSlug)){
                    $exports_page = array(
                        'post_title' => $pluginPage['post_title'],
                        'post_content' => $pluginPage['post_content'],
                        'post_status' => 'publish',
                        'post_type' => 'page',
                        'post_author' => 1,
                        'post_date' => date('Y-m-d H:i:s'),
                        'post_name' => $pluginSlug,
                        'meta_input' => array('_wp_page_template' => $pluginPage['_wp_page_template'])
                    );				
                    wp_insert_post($exports_page);
                }				
            }

    }

    return $pluginsPages;
}
        
        
