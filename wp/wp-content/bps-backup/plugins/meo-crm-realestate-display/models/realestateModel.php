<?php

class RealestateModel
{ 
    // Init table variables
    public static $table_post = 'wp_posts';
    
    public static $table_lot = 'wp_meo_crm_realestate_lots'; 
    public static $table_plan = 'wp_meo_crm_realestate_plans';
    public static $table_floor = 'wp_meo_crm_realestate_floors';
    public static $table_status = 'wp_meo_crm_realestate_status'; 
    public static $table_sector = 'wp_meo_crm_realestate_sectors';
    public static $table_meta = 'wp_meo_crm_realestate_lot_metas';  
    public static $table_spinner = 'wp_meo_crm_realestate_spinners'; 
    public static $table_building = 'wp_meo_crm_realestate_buildings'; 
    public static $table_floor_lot = 'wp_meo_crm_realestate_floor_lot'; 
    public static $table_meta_value = 'wp_meo_crm_realestate_lot_meta_value';
    public static $table_post_spinner = 'wp_meo_crm_realestate_spinner_post'; 
    public static $table_developpement = 'wp_meo_crm_realestate_developpements';
    public static $table_plan_lot_building = 'wp_meo_crm_realestate_plan_lot_building';
    public static $table_coordinates_plan_lot_building = 'wp_meo_crm_realestate_coordinates_plan_lot_building';
    public static $table_link_coordinates_plan_floor_lot_building = 'wp_meo_crm_realestate_link_coordinates_plan_floor_lot_building';   
    
    /*
     * DEVELOPPEMENT QUERY METHOD
     */
    public static function selectDeveloppement()
    {
        global $wpdb;        
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_developpement.' ';        
        return $wpdb->get_results($query);
    }
    public static function selectDeveloppementWhere($wheres = array())
    {
        global $wpdb;        
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_developpement.' ';        
        if(isset($where) && !empty($where))
        {
            $query .= 'WHERE ';
            foreach($wheres as $where)
            {
                $query .= $where['before'].' '.$where['key'].' '.$where['comparator'].' '.$where['value'].' ';
            }
        }
        return $wpdb->get_results($query);
    }
    
    /*
     * SECTOR QUERY METHOD
     */
    public static function selectSector()
    {
        global $wpdb;        
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_sector.' ';        
        return $wpdb->get_results($query);
    }
    public static function selectSectorWhere($wheres = array())
    {
        global $wpdb;        
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_sector.' ';        
        if(isset($where) && !empty($where))
        {
            $query .= 'WHERE ';
            foreach($wheres as $where)
            {
                $query .= $where['before'].' '.$where['key'].' '.$where['comparator'].' '.$where['value'].' ';
            }
        }        
        return $wpdb->get_results($query);
    }
    
    /*
     * BUILDING QUERY METHOD
     */
    public static function selectBuilding()
    {
        global $wpdb;        
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_building.' ';        
        $query  .= 'ORDER BY title ASC ';        
        return $wpdb->get_results($query);
    }
    public static function selectBuildingById($id)
    {
        global $wpdb;        
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_building.' ';        
        $query  .= 'WHERE id='.$id.' ';        
        return $wpdb->get_results($query);
    }
    public static function selectBuildingWhere($wheres = array())
    {
        global $wpdb;        
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_building.' ';        
        if(isset($where) && !empty($where))
        {
            $query .= 'WHERE ';
            foreach($wheres as $where)
            {
                $query .= $where['before'].' '.$where['key'].' '.$where['comparator'].' '.$where['value'].' ';
            }
        }    
        $query  .= 'ORDER BY title ASC ';
        return $wpdb->get_results($query);
    }
    
    /*
     * SELECT ALL DATAS
     */
    public static function selectLot()
    {
        global $wpdb;        
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_lot.' ';
        $query  .= 'ORDER BY title ASC ';
        return $wpdb->get_results($query);
    }
    
        
    public static function selectLotRooms()
    {
        global $wpdb;        
        $query   = 'SELECT l.rooms ';
        $query  .= 'FROM '.self::$table_lot.' AS l ';           
        $query  .= 'GROUP BY l.rooms ';        
        $query  .= 'ORDER BY l.rooms ASC ';        
        return $wpdb->get_results($query);
    }
    
    public static function selectLotSurface()
    {
        global $wpdb;        
        $query   = 'SELECT l.surface ';
        $query  .= 'FROM '.self::$table_lot.' AS l ';           
        $query  .= 'GROUP BY l.surface ';        
        $query  .= 'ORDER BY l.surface ASC ';        
        return $wpdb->get_results($query);
    }
    
    public static function selectMeta()
    {
        global $wpdb;        
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_meta.' ';        
        $query  .= 'ORDER BY meta_order ASC';
        return $wpdb->get_results($query);
    }
    
    public static function selectMetaValue()
    {
        global $wpdb;        
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_meta_value.' ';        
        return $wpdb->get_results($query);
    }
    
    public static function selectFloor($groupBy = false)
    {
        global $wpdb;        
        $query  = 'SELECT * ';
        $query .= 'FROM '.self::$table_floor.' ';
        if($groupBy)
        {
            $query .= 'GROUP BY position ';
        }
        $query .= 'ORDER BY position ASC ';        
        return $wpdb->get_results($query);
    }
    
    public static function selectFloorById($id)
    {
        global $wpdb;        
        $query  = 'SELECT * ';
        $query .= 'FROM '.self::$table_floor.' ';
        $query .= 'WHERE id = '.$id.' ';        
        return $wpdb->get_results($query);
    }
    
    public static function selectPlan()
    {
        global $wpdb;        
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_plan.' ';        
        return $wpdb->get_results($query);
    }
    
    public static function selectStatus()
    {
        global $wpdb;        
        $query   = 'SELECT * 
                    FROM '.self::$table_status.' 
                    ORDER BY name';
        return $wpdb->get_results($query);
    }
    
    public static function selectFloorLot()
    {
        global $wpdb;        
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_floor_lot.' ';        
        return $wpdb->get_results($query);
    }
    
    public static function selectCoordinatePlanLotBuilding()
    {
        global $wpdb;        
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_coordinates_plan_lot_building.' ';        
        return $wpdb->get_results($query);
    }    
    public static function selectLinkCoordinatePlanFloorLotBuilding()
    {
        global $wpdb;        
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_link_coordinates_plan_floor_lot_building.' ';        
        return $wpdb->get_results($query);
    }    
    public static function selectPlanFloorLotBuilding()
    {
        global $wpdb;        
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_plan_lot_building.' ';        
        return $wpdb->get_results($query);
    }
    
    public static function selectCoordinatePlanLotBuildingByFloorLotId($id,$status_id)
    {
        global $wpdb;        
        $query   = 'SELECT cplb.id, plb.image, plb.front_face ';
        $query  .= 'FROM '.self::$table_coordinates_plan_lot_building.' AS cplb ';        
        $query  .= 'LEFT JOIN '.self::$table_link_coordinates_plan_floor_lot_building.' AS lcpflb ON lcpflb.coordinates_id = cplb.id ';        
        $query  .= 'LEFT JOIN '.self::$table_plan_lot_building.' AS plb ON plb.id = lcpflb.plan_lot_id ';        
        $query  .= 'WHERE cplb.id = '.$id.' ';        
        $query  .= 'AND  plb.status_id = '.$status_id.' ';        
        $query  .= 'AND  plb.hover = 0 ';        
        return $wpdb->get_results($query);
    }   
    
    
    /*
     * BUILDINGS + FULL LOTS QUERY METHOD
     * 
     * Generate a big array with all developpement informations
     * 
     */
    public static function selectBuildingsWithLot()
    {
        global $wpdb;
        $count = 0;
        $datas = array();
        $meoUtilities = new MeoScCf7Utilities;
        
        $temp_lot = 0;
        $temp_status = 0;
        $temp_lot_total = 0;
        $sectors = self::selectSector();
        $buildings = self::selectBuilding();
        $lots = self::selectLot();
        $status = self::selectStatus();
        $metas = self::selectMeta();
        $metas_value = self::selectMetaValue();
        $plans = self::selectPlan();        
        $floors = self::selectFloor();
        $floors_lot = self::selectFloorLot();        
        $coordinate_plan_lot_buildings = self::selectCoordinatePlanLotBuilding();
        $link_coordinate_plan_lot_buildings = self::selectLinkCoordinatePlanFloorLotBuilding();
        $plan_lot_buildings = self::selectPlanFloorLotBuilding();
        
        foreach($buildings as $building)
        {
            foreach($status as $value)
            {
                $datas['buildings'][$building->id]['status_lot'][$value->name]['value'] = 0;
                $datas['buildings'][$building->id]['status_lot'][$value->name]['color'] = $value->color;
            }
        
            $datas['buildings'][$building->id]['count_lots'] = 0;
            $datas['buildings'][$building->id]['title'] = $building->title;
            $datas['buildings'][$building->id]['description'] = $building->description;
            $datas['buildings'][$building->id]['coordinate_plan_sector_2d'] = $building->coordinate_plan_sector_2d; 
            $datas['buildings'][$building->id]['image_hover_plan_sector_2d'] = $building->image_hover_plan_sector_2d;             
            $datas['buildings'][$building->id]['image_front_face'] = $building->image_front_face; 
            $datas['buildings'][$building->id]['image_back_face'] = $building->image_back_face;             
            $datas['buildings'][$building->id]['image_preview'] = $building->image_preview; 
            $datas['buildings'][$building->id]['image_building_sector'] = $building->image_building_sector; 
            
            
            foreach($sectors as $sector)
            {
                if($sector->id == $building->sector_id)
                {
                    $datas['buildings'][$building->id]['sector'] = $sector->title;
                    $datas['buildings'][$building->id]['plan_sector_2d'] = $sector->plan_sector_2d;
                }                
            } 
            
            foreach($floors as $floor)
            {
                if($building->id == $floor->building_id)
                {
                    $datas['buildings'][$building->id]['floors'][]['title'] = $floor->title;        
                    
                    foreach($floors_lot as $floor_lot)
                    {                         
                        if($floor_lot->floor_id == $floor->id)
                        {
                            foreach($lots as $lot)
                            {
                                if($floor_lot->lot_id == $lot->id)
                                {
                                     if($temp_lot_total != $lot->id)
                                    {
                                        $datas['buildings'][$building->id]['count_lots']++;
                                        $temp_lot_total = $lot->id;
                                    }
                                    //* LOTS *//
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['document_id'] = MeoCrmCoreCryptData::encodeAttachmentId($lot->document_id);
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['title'] = $lot->title;
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['description'] = $lot->description;
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['room'] = $lot->rooms;
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['surface'] = $lot->surface;
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['type_lot'] = $lot->type_lot;
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['price'] = $lot->price;

                                    foreach($metas_value as $key_meta_value => $meta_value)
                                    {
                                        if($lot->id == $meta_value->lot_id)
                                        {
                                            foreach($metas as $meta)
                                            {
                                                if($meta->id == $meta_value->meta_id)
                                                {
                                                    $datas['buildings'][$building->id]['lots'][$lot->id]['meta_values'][$key_meta_value]['value'] = $meta_value->value;
                                                    $datas['buildings'][$building->id]['lots'][$lot->id]['meta_values'][$key_meta_value]['key'] = $meta->meta_key;
                                                    $datas['buildings'][$building->id]['lots'][$lot->id]['meta_values'][$key_meta_value]['type'] = $meta->meta_type;
                                                    $datas['buildings'][$building->id]['lots'][$lot->id]['meta_values'][$key_meta_value]['slug'] = $meta->meta_slug;
                                                }    
                                            }
                                        }
                                    }
                                    
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['floor_lot'][$floor->id]['floor'] = $floor->title;
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['floor_lot'][$floor->id]['entry'] = $floor_lot->lot_entry;
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['floor_lot'][$floor->id]['position'] = $floor->position;
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['floor_lot'][$floor->id]['plan'] = $floor_lot->plan_floor;
                                    
                                    foreach($plans as $plan)
                                    {
                                        if($plan->id == $lot->plan_id)
                                        {
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan']['title'] = $plan->title;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan']['description'] = $plan->description;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan']['document'] = $plan->document;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan']['plan_2d'] = $plan->plan_2d;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan']['plan_3d'] = $plan->plan_3d;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan']['spinner_id'] = $plan->spinner_id;
                                        }
                                    }
                                    
                                    $count = 0;
                                    foreach($coordinate_plan_lot_buildings as $coordinate_plan)
                                    {
                                        if($coordinate_plan->id == $floor_lot->status_plan_floor_id)
                                        {
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan_building_status'][$count]['title'] = $coordinate_plan->title;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan_building_status'][$count]['coordinate_plan_face'] = $coordinate_plan->coordinates_front;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan_building_status'][$count]['coordinate_plan_back'] = $coordinate_plan->coordinates_back;
                                            
                                            foreach($link_coordinate_plan_lot_buildings as $link_coordinate_plan)
                                            {
                                        
                                                if($link_coordinate_plan->coordinates_id == $coordinate_plan->id)
                                                {
                                                    foreach($plan_lot_buildings as $plan_lot_building)
                                                    {
                                                        $count_image = 0;
                                                        if($plan_lot_building->id == $link_coordinate_plan->plan_lot_id)
                                                        {   
                                                            if($lot->status_id == $plan_lot_building->status_id)
                                                            {           
                                                                if(!empty($plan_lot_building->front_face))
                                                                {
                                                                    if(!empty($plan_lot_building->hover))
                                                                    {
                                                                        $datas['buildings'][$building->id]['lots'][$lot->id]['plan_building_status'][$count]['images_plans'][$count_image]['front']['hover'] = $plan_lot_building->image;
                                                                    }else{
                                                                        $datas['buildings'][$building->id]['lots'][$lot->id]['plan_building_status'][$count]['images_plans'][$count_image]['front']['base'] = $plan_lot_building->image;
                                                                    }   
                                                                }else{
                                                                   if(!empty($plan_lot_building->hover))
                                                                    {
                                                                        $datas['buildings'][$building->id]['lots'][$lot->id]['plan_building_status'][$count]['images_plans'][$count_image]['back']['hover'] = $plan_lot_building->image;
                                                                    }else{
                                                                        $datas['buildings'][$building->id]['lots'][$lot->id]['plan_building_status'][$count]['images_plans'][$count_image]['back']['base'] = $plan_lot_building->image;
                                                                    }    
                                                                }                                                                    
                                                            }
                                                            
                                                        }
                                                        $count_image++;
                                                    }
                                                }
                                            }
                                            
                                        }
                                        $count++;
                                        
                                    }
                                    
                                    /* 
                                     *  $coordinate_plan_lot_buildings = self::selectCoordinatePlanLotBuilding();
                                     *  $link_coordinate_plan_lot_buildings = self::selectLinkCoordinatePlanFloorLotBuilding();                                     *
                                     *  $coordinate_plan_lot_buildings = self::selectPlanFloorLotBuilding();
                                     */     
                                    $lot_status_count = 1;                                    
                                    foreach($status as $value)
                                    {
                                        if($value->id == $lot->status_id)
                                        { 
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['status'] = $value->name;   
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['status_color'] = $value->color;   
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['hide_price'] = $value->hide_price;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['sold'] = $value->sold;
                                            $datas['buildings'][$building->id]['status_lot'][$value->name]['value']++;
                                            $temp_lot = $lot->id;
                                            $temp_status = $lot->status_id;
                                        }
                                    }
                                }
                            } 
                        }
                    }
                }
            }
        }
        
        return $datas;
        
    }
    
    public static function get_development($param = array('building' => 1, 'lot' => 1))
    {
        $count = 0;
        $datas = array();        
                
        if($param['building'])
        {
            $buildings = self::selectBuilding();
            foreach($buildings as $building)
            {
                $datas['buildings'][$count] = array(
                    'id' => $building->id,            
                    'title' => $building->title,            
                    'description' => $building->description,            
                    'coordinate_plan_sector_3d' => $building->coordinate_plan_sector_3d,            
                    'image_hover_plan_sector_3d' => $building->image_hover_plan_sector_3d,            
                    'coordinate_plan_sector_2d ' => $building->coordinate_plan_sector_2d,            
                    'image_hover_plan_sector_2d' => $building->image_hover_plan_sector_2d,            
                    'image_front_face' => $building->image_front_face,            
                    'image_back_face' => $building->image_back_face,            
                    'image_preview' => $building->image_preview,            
                    'image_building_sector' => $building->image_building_sector,            
                    'park_place_outdoor' => $building->park_place_outdoor,            
                    'park_place_indoor' => $building->park_place_indoor         
                );            
                $count++;
            }
        }
        
        if($param['lot'])
        {
            $count = 0;            
            $lots = self::selectLot();
            $list_status = self::selectStatus();
            $metas = self::selectMeta();
            $metas_value = self::selectMetaValue();
            $plans = self::selectPlan();        
            $floors = self::selectFloor();
            $floors_lot = self::selectFloorLot();             
            foreach($lots as $lot)
            {
                $datas['lots'][$count] = array(
                    'id' => $lot->id,
                    'title' => $lot->title,
                    'description' => $lot->description,
                    'rooms' => $lot->rooms,
                    'surface' => $lot->surface,
                    'type_lot' => $lot->type_lot,
                    'price' => $lot->price
                );
                foreach($list_status as $status)
                {
                    if($status->id == $lot->status_id)
                    {
                        $datas['lots'][$count]['status'] = $status->name;
                        $datas['lots'][$count]['status_color'] = $status->color;
                        $datas['lots'][$count]['status_price_enabler'] = 1;
                    }
                }
                foreach($plans as $plan)
                {
                    if($plan->id = $lot->plan_id)
                    {
                        $datas['lots'][$count]['plan']['spinner_id'] = $status->name;
                        $datas['lots'][$count]['plan']['title'] = $status->title;
                        $datas['lots'][$count]['plan']['description'] = $status->description;
                        $datas['lots'][$count]['plan']['document'] = $status->document;
                        $datas['lots'][$count]['plan']['plan_2d'] = $status->plan_2d;
                        $datas['lots'][$count]['plan']['plan_3d'] = $status->plan_3d;
                    }
                }
                foreach($metas_value as $meta_value)
                {
                    foreach($metas as $meta)
                    {
                        if($meta->id == $meta_value->meta_id && $meta_value->lot_id == $lot->id)
                        {
                            $datas['lots'][$count]['metas'][] = array(
                                'meta_key' => $meta->meta_key,
                                'meta_slug' => $meta->meta_slug,
                                'meta_value' => $meta_value->value
                            );
                        }
                    }
                }
                
                foreach($floors_lot as $floor_lot)
                {
                    if($floor_lot->lot_id == $lot->id)
                    {
                        foreach($floors as $floor)
                        {
                            if($floor->id == $floor_lot->floor_id)
                            {
                                $datas['lots'][$count]['floors'][] = array(
                                    'title' => $floor->title,
                                    'building_id' => $floor->building_id,
                                    'position' => $floor->position,
                                    'entry' => $floor_lot->lot_entry,
                                    'plan' => $floor_lot->plan_floor,
                                );
                            }
                        }
                    }
                }

                $count++;
            }
        }
        
        $datas['status'] = self::selectStatus();
        
        return json_encode($datas);        
    }
    
    public static function selectListLotFilter($room, $floor, $building, $surface, $metas = array())
    {
        global $wpdb;    
        
        $query  = 'SELECT l.id ';
        $query .= 'FROM '.self::$table_lot.' AS l ';
        
        if(!empty($metas))
        {
            for($i=0; $i < count($metas); $i++){
                $query .= 'INNER JOIN '.self::$table_meta_value.' AS mv'.$i.' ON l.id = mv'.$i.'.lot_id ';
            }
        }
        
        $query .= 'INNER JOIN '.self::$table_floor_lot.' AS fl ON l.id = fl.lot_id ';
        $query .= 'INNER JOIN '.self::$table_floor.' AS f ON f.id = fl.floor_id ';
        
        if(!empty($room) || ($floor != '' && $floor != null) || !empty($building) || !empty($surface) || (is_array($metas) && count($metas) != 0) ){
            $query  .= 'WHERE  '; 
            
            if(!empty($room))
            {
                $query .= 'l.rooms = '.$room.' ';
            }
            
            if($floor != '' && $floor != null)
            {
                $query .= (!empty($room)) ? 'AND ' : '';
                $query .= 'f.position = '.$floor.' ';
            }
            
            if(!empty($building))
            {
                $query .= (!empty($room) || !empty($floor)) ? 'AND ' : '';
                $query .= 'f.building_id = '.$building.' ';
            }
            
            if(!empty($surface))
            {
                $pattern = '/^[0-9]+\\-[0-9]+$/';
                $query .= (!empty($room) || !empty($floor) || !empty($building)) ? 'AND ' : '';
                
                if(preg_match($pattern,$surface))
                {
                    list($surface_1, $surface_2) = explode('-',$surface);
                    $query .= 'l.surface BETWEEN '.$surface_1.' AND '.$surface_2.' ';
                }else{
                    $query .= 'l.surface >= '.$surface.' ';
                } 
            }
            
            if(is_array($metas) && count($metas) != 0)
            {
                if(!empty($room) || ($floor != '' && $floor != null) || !empty($building) || !empty($surface))
                {
                    $query .= 'AND ';
                }                
                $count = 0;
                foreach($metas as $key => $meta){                    
                    if($meta != '' && $meta != null){
                        $query .= ($count > 0) ? 'AND ' : '' ;
                        if($meta == 'true' || $meta == 'false')
                        {
                            if($meta == 'false')
                            {
                                $query .= '((mv'.$count.'.meta_id = '.substr($key, 3).' AND mv'.$count.'.value = 0) ';
                                $query .= 'OR (mv'.$count.'.meta_id = '.substr($key, 3).' AND mv'.$count.'.value = "")) ';
                            }else{
                                $query .= '((mv'.$count.'.meta_id = '.substr($key, 3).' AND mv'.$count.'.value > 0) ';
                                $query .= 'OR (mv'.$count.'.meta_id = '.substr($key, 3).' AND mv'.$count.'.value != "")) ';
                            }
                            
                        }else{
                            $query .= '(mv'.$count.'.meta_id = '.substr($key, 3).' AND mv'.$count.'.value = '.$meta.') ';
                        }
                        $count++;
                    }
                }
            }
        }
        
        $query .= 'GROUP BY l.id ';
        
        return $wpdb->get_results($query);
    }
    
    public static function selectFilterMetaValue()
    {
        global $wpdb;  
        $count = 0;
        $list_id = '';
        $datas = array();
        
        // Query for select meta filter
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_meta.' ';        
        $query  .= 'WHERE meta_filter = 1 ';        
        $query  .= 'ORDER BY meta_order ASC ';        
        $metas = $wpdb->get_results($query);
        
        // Create a string with all meta_id filter
        foreach($metas as $meta)
        {
            if($count > 0)
            {
                $list_id .= ',';
            }
            $list_id .= $meta->id;
            $count++;
        }
        
        // Query for select a meta value link to meta
        $query  = 'SELECT * ';
        $query .= 'FROM '.self::$table_meta_value.' '; 
        $query .= 'WHERE meta_id IN ('.$list_id.') ';
        $query .= 'GROUP BY value, meta_id ';
        $meta_values = $wpdb->get_results($query);
        
        foreach($metas as $meta)
        {
            $datas[$meta->id]['id'] = $meta->id;
            $datas[$meta->id]['key'] = $meta->meta_key;
            $datas[$meta->id]['slug'] = $meta->meta_slug;
            $datas[$meta->id]['type'] = $meta->meta_type;
            foreach($meta_values as $meta_value)
            {
                if($meta_value->meta_id == $meta->id)
                {
                    $datas[$meta->id]['values'][] = $meta_value->value;
                }
            }   
        }
        
        return $datas;
    }
    
    /*
     * SELECT LOT BY ID
     */
    public static function selectLotById($id)
    {
        global $wpdb;    
        $lot = array();
        $previousMetaId = 0;
        
        $query   = 'SELECT l.id AS lot_id, l.document_id, l.title AS lot, l.description, l.status_id, l.rooms, l.surface, l.type_lot, mv.value, mv.meta_id, m.meta_slug, m.meta_key, m.meta_order, p.plan_2d, p.plan_3d, s.name AS status, fl.floor_id, fl.plan_floor, fl.status_plan_floor_id ';
        $query  .= 'FROM '.self::$table_lot.' AS l ';        
        $query  .= 'LEFT JOIN '.self::$table_meta_value.' AS mv ON mv.lot_id = l.id ';        
        $query  .= 'LEFT JOIN '.self::$table_meta.' AS m ON m.id = mv.meta_id ';        
        $query  .= 'LEFT JOIN '.self::$table_floor_lot.' AS fl ON fl.lot_id = l.id ';                 
        $query  .= 'LEFT JOIN '.self::$table_plan.' AS p ON p.id = l.plan_id ';         
        $query  .= 'LEFT JOIN '.self::$table_status.' AS s ON s.id = l.status_id ';         
        $query  .= 'WHERE l.id='.$id.' '; 
                
        $results = $wpdb->get_results($query);
        
        foreach($results as $result)
        {
            $lot['title'] = $result->lot;
            $lot['description'] = $result->description;
            $lot['rooms'] = $result->rooms;
            $lot['surface'] = $result->surface;
            $lot['type_lot'] = $result->type_lot;
            $lot['document'] = $result->document;
            $lot['plan_2d'] = $result->plan_2d;
            $lot['plan_3d'] = $result->plan_3d;
            $lot['status'] = $result->status;
            $lot['price'] = $result->price;

            $lot['metas'][$result->meta_id]['label'] = $result->meta_key;
            $lot['metas'][$result->meta_id]['slug'] = $result->meta_slug;
            $lot['metas'][$result->meta_id]['order'] = $result->meta_order;
            $lot['metas'][$result->meta_id]['value'] = $result->value; 
            
            $floor = self::selectFloorById($result->floor_id);            
            $lot['floors'][$result->floor_id]['title'] = $floor[0]->title;
            $lot['floors'][$result->floor_id]['position'] = $floor[0]->position;            
            $lot['floors'][$result->floor_id]['plan'] = $result->plan_floor;   
            
            $buildings = self::selectBuildingById($floor[0]->building_id);
            
            foreach($buildings as $building)
            {
                $lot['building']['name'] = $building->title;            
                $lot['building']['plan_sector'] = $building->image_building_sector;            
                $lot['building']['building_front'] = $building->image_front_face;            
                $lot['building']['building_back'] = $building->image_back_face;                     
            }
            
            if(!empty($result->status_plan_floor_id))
            {
                $plans_lot = self::selectCoordinatePlanLotBuildingByFloorLotId($result->status_plan_floor_id, $result->status_id);            
                foreach($plans_lot as $plan_lot){                
                    $lot['plans_lot_building'][$plan_lot->id][$plan_lot->front_face]['image'] = $plan_lot->image;                
                } 
            }
        }        
        return $lot;
    }
    
}