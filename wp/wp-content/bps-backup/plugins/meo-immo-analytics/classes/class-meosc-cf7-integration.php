<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoScCf7Integration {

	private $smartcapture = null;
	private $utilities = null;


	// Build the instance
	public function __construct($smartcapture, $utilities) {
		$this->smartcapture = $smartcapture;
		$this->utilities = $utilities;
	}

	// Prepare hooks, filters, actions and shortcodes
	public function addHooks() {
		add_action( 'init', array( $this, 'mscWpcf7AddShortcode' ) );
		add_action( 'init', array( $this, 'mscCf7dbAddHooks' ) );
	}

	# Add shortcodes / filters / actions
	
	public function mscWpcf7AddShortcode() {
		
		# Should always exist, as there's an explicit dependency between the plugins
		if (function_exists('wpcf7_add_shortcode')) {
			
			# Add fields
			
			wpcf7_add_shortcode('file_id',  array($this, 'mscCf7FieldShortcode' ), true);
			wpcf7_add_shortcode('file_id*', array($this, 'mscCf7FieldShortcode' ), true);
			
			wpcf7_add_shortcode('post_id',  array($this, 'mscCf7FieldShortcode' ), true);
			wpcf7_add_shortcode('post_id*', array($this, 'mscCf7FieldShortcode' ), true);

      wpcf7_add_shortcode('lot_id',  array($this, 'mscCf7FieldShortcode' ), true);
      wpcf7_add_shortcode('lot_id*', array($this, 'mscCf7FieldShortcode' ), true);

      wpcf7_add_shortcode('item',  array($this, 'mscCf7FieldShortcode' ), true);
      wpcf7_add_shortcode('item*', array($this, 'mscCf7FieldShortcode' ), true);
			
			wpcf7_add_shortcode('inputPreviousUrl',  		array($this, 'mscCf7inputPreviousUrlShortcode' ), true);
			wpcf7_add_shortcode('buttonCancelPreviousUrl',  array($this, 'mscCf7buttonCancelPreviousUrlShortcode' ), true);

			# Add fields validation
			add_filter('wpcf7_validate_file_id',  array($this, 'mscCf7ValidateFileId'), 10, 2);
			add_filter('wpcf7_validate_file_id*', array($this, 'mscCf7ValidateFileId'), 10, 2);

			# Handle file request
			add_action('wpcf7_before_send_mail', array($this, 'mscHandleFileRequest'));
		}
	}

	# Hooks specific to the CF7 to DB plugin
	
	public function mscCf7dbAddHooks() {
		
		// Make every effort to get the analytics ID
		add_filter( 'cfdb_form_data', array($this, 'mscCf7dbSetAnalyticsId'), 1);
	}
	
	# File Id and Post Id shortcode
	
	public function mscCf7FieldShortcode($tag) {
		if (!is_array($tag)) {
			return '';
		}
	
		$name = $tag['name'];
		if (empty($name)) {
			return '';
		}
	
		$value = $_GET[$name];
	
		return '<input type="hidden" name="' . $name . '" value="' . $value . '" />';
	}
	
	# TODO :: Recheck the two functions below (from estavayer theme)
	
	/* Shortcode to generate input hidden [inputPreviousUrl url ]
	 * [inputPreviousUrl url=""] */
	
	public function mscCf7inputPreviousUrlShortcode() {
		$url = '';
		if(isset($_GET['current_url']) && !empty($_GET['current_url']))
		{
			$url = 'http://'.$_GET['current_url'];
		}
		return '<input type="hidden" name="pervious_url" value="'.$url.'">';
	}
	
	/* Shortcode to generate button cancel for file request page on mobile device [buttonCancelPreviousUrl url ]
	 * [buttonCancelPreviousUrl url=""] */
	
	public function mscCf7buttonCancelPreviousUrlShortcode() {
	
		$url = '';
		$html = '';
	
		if(isset($_GET['current_url']) && !empty($_GET['current_url']))
		{
			$url = 'http://'.$_GET['current_url'];
			$html = '<a class="wpcf7-button-cancel" href="'.$url.'"><input type="button" value="Annuler" /></a>';
		}
	
		return $html;
	}
	
	
	
	
	

	// Validate file_id field
	public function mscCf7ValidateFileId($result, $tag) {
		$type = $tag['type'];
		$name = $tag['name'];

		$obfuscated_id = (int) $_POST[$name];
		$file_id = MeoCrmCoreCryptData::decodeAttachmentId($obfuscated_id);

		$file = get_post($file_id);

		// test if not use $meoMIMEtypes instead of only pdf
		if ( (empty($file) || $file->post_mime_type != 'application/pdf') && ($type == 'file_id*' || !empty($_POST[$name]))){
			$result['valid'] = false;
			$result['reason'][$name] = wpcf7_get_message( 'invalid_required' );
		}

		return $result;
	}

	/*
	 * Prepare Mailing or not, based on form submission. Your download email and rest
	 */
	public function mscHandleFileRequest($cf7) {

	  global $wpdb;

		$errors = array();

        $upload_dir = wp_upload_dir();

		if (!is_a($cf7, 'WPCF7_ContactForm')) {
			return;
		}

		$submission = WPCF7_Submission::get_instance();
		if (!$submission) {
			return;
		}

		$posted_data = $submission->get_posted_data();

		if (!isset($posted_data['file_id']) or !$posted_data['file_id']
			or !isset($posted_data['pdf-download']) or !$posted_data['pdf-download']) {
				
			# We are supposingly on a simple Contact Form, no download
			# Check if we force SmartCapture
			if(array_key_exists('force_smartcapture', $posted_data) && $posted_data['force_smartcapture'] == 'true'){

        // GET OPTIONS
        $smart_capture_url = get_option('smart_capture_url');
        $api_key = get_option('api_key');

        $schedule_url = $smart_capture_url . "/wp/wp-admin/admin-ajax.php";

        $data = [
            'action' => 'check_contact_exist',
            'email' => $posted_data['email'],
            'api_key' => $api_key,
            'submit_time' => $posted_data['submit_time'],
            'message' => $posted_data['message']
        ];

        $args = array(
            'method' => 'POST',
            'headers' => array(),
            'body' => $data,
        );

        $response = wp_remote_post( $schedule_url, $args );

        $body = json_decode($response['body']);

        if(!$body->success) {
          MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, 'ERROR : ' . $body->message);
        }

				$this->smartcapture->schedulePdfEmail($cf7, $posted_data);

			}
			
			return;
			
		}

		// Get the id and decode it
		$obfuscated_id = (int) $posted_data['file_id'];
		$file_id = MeoCrmCoreCryptData::decodeAttachmentId($obfuscated_id);

		// Lot ID is optional
		$lot_id = (int) $_GET['post_id'];
		$lot = get_post($lot_id);
		if (!empty($lot)) {
			// Set lot code in email sent to developer
			$mail = $cf7->prop( 'mail' );
			$mail['body'] = str_replace('[lot_code]',__($lot->post_title), $mail['body']);
			$cf7->set_properties( array( 'mail' => $mail ) );
		}

		// Starting to build the email
		$download_url = $this->smartcapture->getDownloadLink($cf7, $posted_data, $file_id);
		
		$email_sender      = get_option('email_sender');
		$email_sender_name = get_option('email_sender_name');
		$email_bcc         = get_option('email_bcc');
		$logo              = get_option('logo');
		
		list($email_subject, $email_content) = $this->getEmailContent($file_id, $lot, $posted_data);

		if (empty($email_content)) {
			return;
		}

		$logo_tag = '';
		if (!empty($logo)) {
			$logo_tag = '<img src="' . $logo . '" />';
		}


		$replacements = array(
								'[download_url]'   => '<a href="'.$download_url.'" title="t&eacute;l&eacute;charger le fichier">t&eacute;l&eacute;charger le fichier'."</a>",
								'[lot_code]'       => __($lot->post_title),
								'[signature]'      => get_option('signature'),
                '[logo]'           => $logo_tag,
								'[signature_logo]' => '<img src="' . $upload_dir['baseurl'] . '/' . get_option('signature_logo') . '" alt="" />',
								'[sender_email]'   => $email_sender,
                                '[url_3d]'         => '<a href="http://custimmo.com/biens-immobiliers/?ps_keyword=&ps_status=-1&ps_location=Bramois&ps_type=-1&ps_style=-1&ps_rooms=-1&ps_bedrooms=-1&ps_bathrooms=-1&ps_garages=-1&ps_area_min=0&ps_area_max=400&ps_area_big=400">ce lien</a>'
							);

		foreach ($replacements as $replacement_code => $replacement_value) {
			$email_subject = str_replace($replacement_code, $replacement_value, $email_subject);
			$email_content = str_replace($replacement_code, $replacement_value, $email_content);
		}

		/*foreach ($posted_data as $posted => $data) {
			$email_content .= '<br/>'.$posted.' : '.$data.'<br/>';
		}*/		
		// $email_content .= '<br/>'.$posted.' : '.$data.'<br/>';
		// $email_content .= '<br/>'.$posted.' : '.$data.'<br/>';
		// $email_content .= '<br/>'.$posted.' : '.$data.'<br/>';
		// $email_content .= '<br/>'.$posted.' : '.$data.'<br/>';
		
		$headers = array(
			'From: ' . $email_sender_name . ' <' . $email_sender . '>',
			'Content-type: text/html; charset="utf-8"'
		);

		if (!empty($email_bcc)) {
			$headers[] = 'Bcc: ' . $email_bcc;
		}
		
		// Send email
		if(!wp_mail($posted_data['email'], $email_subject, $email_content, $headers))
		{
			$errors[] = 'erreur de l\'envoie du mail de telechargement !!';				
			MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Error download mail 1 " . $errors[0]);
		}
		
		// Update : if everything went fine, we set a Session value to print prices over the front end (only if price list file has been downloaded)
		$this->mscCheckDownloadedItem($file_id);
		
	}

	// Function to verify if the downloaded media is the price list one
	public function mscCheckDownloadedItem($decodedMediaID){
	
		// Check if it's a price enabler
		if(get_post_meta($decodedMediaID, "_price_enabler", true)) { 
			
			// Enable prices on the frontend for the current session
			$_SESSION['pricesEnabled'] = true;
			return true;
		}
		else return false;
	}

	public static function isPriceEnabled()
    {
        return isset($_SESSION["pricesEnabled"]) && $_SESSION["pricesEnabled"];
    }

	private function getEmailContent($file_id, $lot, $posted_data) {
		// email content may be in one of three places
		// 1. the attachment itself
		// 2. the post's taxonomy eg lot type
		// 3. the post containing the form (most general - usually a fallback)

		$email_subject = get_option('email_subject');
		$email_content = get_option('email_content');

		if (!empty($email_content)) {
			return array($email_subject, wpautop($email_content));
		}

		if (!empty($lot)) {
			$taxonomies =  get_taxonomies();
			foreach ($taxonomies as $taxonomy) {
				$terms = wp_get_post_terms( $lot->ID, $taxonomy);
				if (empty($terms)) {
					continue;
				}
				foreach ($terms as $term) {
					$email_subject = get_option('email_subject');
					$email_content = get_option('email_content');

					if (!empty($email_content)) {
						return array($email_subject, wpautop($email_content));
					}
				}
			}
		}


		// Get the ID of the page the form was on.  Matches logic in wpcf7_special_mail_tag_for_post_data()
		if ( ! preg_match( '/^wpcf7-f(\d+)-p(\d+)-o(\d+)$/', $posted_data['_wpcf7_unit_tag'], $matches ) ) {
			return array(null, null);
		}
		$post_id = (int) $matches[2];

		$email_subject = get_option('email_subject');
		$email_content = get_option('email_content');

		return array($email_subject, wpautop($email_content));
	}

	public function mscCf7dbSetAnalyticsId($cf7) {

		if (isset($cf7->posted_data['analytics_id'])) {
			return $cf7;
		}

		require_once( dirname(__FILE__) . '/class-meosc-piwik-integration.php');
		$analytics_id = MeoPiwikIntegration::mscPiwikGetAnalyticsIdFromCookie();

		if (!empty($analytics_id)) {
			$cf7->posted_data['analytics_id'] = $analytics_id;
		}

		return $cf7;
	}
}
