<?php
/*
 * Template name: TPL Blog
 */

$args = array(
  'posts_per_page'   => -1,
  'orderby'          => 'date',
  'order'            => 'DESC',
);

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['template_path'] = get_template_directory_uri();
$context['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));
$context['disclamer'] = get_field('disclamer', 'option');
$context['full_content'] = get_field('full_content',$post->ID);

$articles = get_posts( $args );
$context['articles'] = array();

foreach($articles as $key => $article) {

  $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $article->ID ), 'full' );
  $article->image_featured = $featured_image[0];

}

$context['articles'] = $articles;

/*echo '<pre>';
print_r($context['articles']);
echo '</pre>'; exit();*/

$context['meta_title'] = get_field('title',$post->ID);
$context['meta_description'] = get_field('description',$post->ID);

Timber::render( array( 'page-' . $post->post_name . '.twig', 'blog.html.twig' ), $context );