<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/views/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['template_path'] = get_template_directory_uri();
$context['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));

$context['disclamer'] = get_field('disclamer', 'option');

$context['meta_title'] = get_field('title',$post->ID);
$context['meta_description'] = get_field('description',$post->ID);

Timber::render( array( 'page-' . $post->post_name . '.twig', 'page.twig' ), $context );