<?php
/*
 * Template name: TPL Homepage
 */

$post = new TimberPost();
$context = Timber::get_context();
$context['post'] = $post;
$context['template_path'] = get_template_directory_uri();
$context['slider'] = (have_rows('slider')) ? get_field('slider') : array();
$context['url_plan'] = site_url( '/list-lots/' );
$context['url_contact'] = site_url( '/contact/' );
$context['url_concept'] = site_url( '/concept/' );
$context['navigation_home'] = wp_nav_menu(array('menu' => 'homepage_nav', 'echo' => false));
$context['analytics_id'] = $_COOKIE['analytics_id'];
$context['disclamer'] = get_field('disclamer', 'option');

$context['meta_title'] = get_field('title',$post->ID);
$context['meta_description'] = get_field('description',$post->ID);


Timber::render( array( 'page-' . $post->post_name . '.twig', 'homepage.twig' ), $context );