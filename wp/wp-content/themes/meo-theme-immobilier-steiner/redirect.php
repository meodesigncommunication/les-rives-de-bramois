<?php
/*
 * Template name: TPL redirect
 */

global $post;

$url = get_field('url', $post->ID);

header("Status: 301 Moved Permanently", false, 301);

header("Location: ".$url);

exit();

?>