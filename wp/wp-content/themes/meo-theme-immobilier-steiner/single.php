<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['disclamer'] = get_field('disclamer', 'option');
$context['template_path'] = get_template_directory_uri();
$context['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));

$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

$context['featured_image'] = $featured_image[0];

$context['meta_title'] = get_field('title',$post->ID);
$context['meta_description'] = get_field('description',$post->ID);

Timber::render( array( 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context );