<?php
/**
 * Template name: Privacy Policy
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['template_path'] = get_template_directory_uri();
$context['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));
$context['disclamer'] = get_field('disclamer', 'option');
$context['meta_title'] = get_field('title',$post->ID);
$context['meta_description'] = get_field('description',$post->ID);

Timber::render( array( 'privacy_policy.twig' ), $context );