<?php
/*
 * Template name: TPL Homepage V2
 */

$post = new TimberPost();
$context = Timber::get_context();
$context['post'] = $post;
$context['template_path'] = get_template_directory_uri();
$context['slider'] = (have_rows('slider')) ? get_field('slider') : array();
$context['url_plan'] = site_url( '/list-lots/' );
$context['url_contact'] = site_url( '/contact/' );
$context['url_concept'] = site_url( '/concept/' );
$context['navigation_home'] = wp_nav_menu(array('menu' => 'homepage_nav', 'echo' => false));
$context['analytics_id'] = $_COOKIE['analytics_id'];
$context['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));
$context['full_content'] = get_field('full_content',$post->ID);

$context['disclamer'] = get_field('disclamer', 'option');

$context['popup']['enable'] = get_field('enable_popup',$post->ID);
$context['popup']['opened'] = get_field('id_start',$post->ID);
$context['popup']['content'] = get_field('content-popup',$post->ID);

$args = array(
  'posts_per_page'   => -1,
  'orderby'          => 'date',
  'order'            => 'DESC',
);

$articles = get_posts( $args );
$context['articles'] = array();

foreach($articles as $key => $article) {

  $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $article->ID ), 'full' );
  $article->image_featured = $featured_image[0];

}

$context['articles'] = $articles;

$context['meta_title'] = get_field('title',$post->ID);
$context['meta_description'] = get_field('description',$post->ID);

Timber::render( array( 'page-' . $post->post_name . '.twig', 'homepage-v2.twig' ), $context );